package org.ayizan.android.event.model;

import androidx.annotation.DrawableRes;

import org.ayizan.android.fogguide.R;

/**
 * Created by bcholmes on 2018-03-14.
 */

public enum LinkType {
    TWITTER(R.drawable.ic_twitter), BOOK(R.drawable.ic_book),
    LINK(R.drawable.ic_link), DREAMWIDTH(R.drawable.ic_twitter);

    final int icon;

    private LinkType(@DrawableRes int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }
}
