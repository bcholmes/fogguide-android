package org.ayizan.android.event.util;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

import org.ayizan.android.event.util.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONResourceReader {
	
	public JSONObject loadJson(Context context, String resourceName) {
		try {
			return new JSONObject(readJson(context, resourceName));
		} catch (JSONException e) {
			return new JSONObject();
		}
	}

	private String readJson(Context context, String resourceName) {
		try {
			InputStream input = context.getAssets().open(resourceName);
			try {
				return IOUtils.toString(input, "UTF-8");
			} finally {
				IOUtils.closeQuietly(input);
			}
		} catch (IOException e) {
			return "{}";
		}
	}
	

}
