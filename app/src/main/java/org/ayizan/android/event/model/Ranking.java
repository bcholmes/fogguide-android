package org.ayizan.android.event.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Ranking {
	
	private int score;
	private int yayCount;
	private int omgCount;
	
	public int getYayCount() {
		return yayCount;
	}
	public void setYayCount(int yayCount) {
		this.yayCount = yayCount;
	}
	public int getOmgCount() {
		return omgCount;
	}
	public void setOmgCount(int omgCount) {
		this.omgCount = omgCount;
	}
	public int getScore() {
		return score;
	}

	public static Ranking fromJson(JSONObject json) throws JSONException {
		Ranking result = new Ranking();
		result.score = json.has("rank") ? json.getInt("rank") : 0;
		result.yayCount = json.has("yay") ? json.getInt("yay") : 0;
		result.omgCount = json.has("omg") ? json.getInt("omg") : 0;
		return result;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
}
