package org.ayizan.android.event.model;

public interface Groupable {
	public String getGroup();
}
