package org.ayizan.android.event.service;

import android.content.Context;
import android.util.Log;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Location;
import org.ayizan.android.event.model.ScheduledItem;
import org.ayizan.android.event.model.SimpleScheduledItem;
import org.ayizan.android.event.model.TimeRange;
import org.ayizan.android.event.util.JSONResourceReader;
import org.ayizan.android.event.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GridViewService {

    Context context;
    EventService eventService;
    private JSONResourceReader reader = new JSONResourceReader();
    private List<ScheduledItem> miscItems;

    private List<String> days;


    public GridViewService(Context context, EventService eventService) {
        this.context = context;
        this.eventService = eventService;
    }

    public List<Location> getAllLocations() {
        return this.eventService.getAllLocations();
    }

    public List<ScheduledItem> getItemsForDay(String day) {
        List<ScheduledItem> result = new ArrayList<>();
        result.addAll(getMiscItems());
        result.addAll(this.eventService.getTopLevelProgramItems());
        return filterByDay(result, day);
    }

    private List<ScheduledItem> getMiscItems() {
        if (this.miscItems == null) {
            this.miscItems = initializeMiscHours();
        }
        return this.miscItems;
    }

    public List<String> getDays() {
        if (this.days == null) {
            List<TimeRange> ranges = new ArrayList<>();
            for (ScheduledItem e : this.eventService.getLeafProgramItems()) {
                ranges.add(e.getTimeRange());
            }
            for (ScheduledItem i : getMiscItems()) {
                ranges.add(i.getTimeRange());
            }
            Collections.sort(ranges);
            List<String> temp = new ArrayList<>();
            for (TimeRange r : ranges) {
                String day = r.getEffectiveDay();
                if (!temp.contains(day)) {
                    temp.add(day);
                }
            }
            this.days = temp;
        }
        return this.days;
    }

    private List<ScheduledItem> filterByDay(List<ScheduledItem> events, String day) {
        if (events.isEmpty()) {
            return Collections.emptyList();
        } else {
            List<ScheduledItem> result = new ArrayList<>();
            for (ScheduledItem event: events) {
                if (day.equals(event.getTimeRange().getEffectiveDay())) {
                    result.add(event);
                }
            }
            return result;
        }
    }

    public List<ScheduledItem> initializeMiscHours() {
        try {
            JSONObject json = this.reader.loadJson(this.context, "hours.json");
            JSONArray array = json.getJSONArray("items");

            return parseItemList(array);
        } catch (JSONException e) {
            return Collections.emptyList();
        }
    }

    private List<ScheduledItem> parseItemList(JSONArray array) throws JSONException {
        List<ScheduledItem> result = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            JSONObject record = array.getJSONObject(i);
            SimpleScheduledItem item = new SimpleScheduledItem();
            if (record.has("id")) {
                item.setId(record.getString("id"));
            }
            if (record.has("description")) {
                item.setDescription(record.getString("description"));
            }
            if (record.has("location")) {
                item.setLocation(this.eventService.resolveLocation(record.getString("location")));
            }
            if (record.has("href")) {
                item.setHref(record.getString("href"));
            }

            if (record.has("startTime") && record.has("endTime")) {
                TimeRange range = new TimeRange();
                try {
                    range.setStartTime(this.eventService.formatter.parse(record.getString("startTime")));
                    range.setEndTime(this.eventService.formatter.parse(record.getString("endTime")));
                } catch (ParseException e) {
                    Log.e(BaseEventApplication.TAG, "Problem parsing date for event " + item.getId(), e);
                }
                item.setTimeRange(range);
            }
            if (item.getTimeRange() != null && StringUtils.isNotEmpty(item.getDescription()) && StringUtils.isNotEmpty(item.getId())) {
                result.add(item);
            }

        }
        return result;
    }
}
