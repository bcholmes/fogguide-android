package org.ayizan.android.event.model;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.ayizan.android.event.util.RenderContext;
import org.ayizan.android.event.util.DateFlyweight;
import org.ayizan.android.event.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Announcement implements Renderable, TwoLineSummary {

	private String title;
	private Date date;
	private String content;
	private String postedByUser;
	private String id;

	public EntityType getEntityType() {
		return EntityType.ANNOUNCEMENT;
	}
	public String getTitle() {
		return title;
	}
	public Date getDate() {
		return date;
	}
	public String getContent() {
		return content;
	}
	public String getPostedByUser() {
		return postedByUser;
	}

	public static Announcement fromJsonObject(JSONObject json) throws JSONException {
		Announcement result = new Announcement();
		result.id = json.has("ID") ? json.getString("ID") : null;
		result.title = json.has("title") ? json.getString("title") : null;
		result.content = json.has("content") ? json.getString("content") : null;
		
		String dateAsString = json.has("date") ? json.getString("date") : null;
		if (StringUtils.isNotEmpty(dateAsString)) {
			try {
				result.date = DateFlyweight.getIsoFormatter().parse(dateAsString);
			} catch (ParseException e) {
				// ignore it
			}
		}
		
		JSONObject authorBlock = json.has("author") ? json.getJSONObject("author") : null;
		if (authorBlock != null) {
			result.postedByUser = authorBlock.has("name") ? authorBlock.getString("name") : null;
		}
		
		return result;
	}
	public static List<Announcement> fromJsonCollection(JSONArray json) throws JSONException {
		List<Announcement> result = new ArrayList<Announcement>();
		for (int i = 0; i < json.length(); i++) {
			JSONObject temp = json.getJSONObject(i);
			Announcement announcement = fromJsonObject(temp);
			result.add(announcement);
		}
		return result;
	}
	@Override
	public String getId() {
		return this.id;
	}
	@Override
	public String getFullHtml(RenderContext renderContext) {
		StringBuilder builder = new StringBuilder();
		
		builder.append("<html><head><title>").append(getFullTitleNoHtml())
				.append("</title><link rel=\"stylesheet\" type=\"text/css\" href=\"./list.css\" />");
		builder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"./list-ipad.css\" />");
		builder.append("<meta name=\"format-detection\" content=\"telephone=no\">");
		builder.append("</head><body><div class=\"content\"><div class=\"main\">");
		builder.append("<h2>").append(this.title).append("</h2>");
		builder.append(this.content);
		builder.append("</div></body></html>");
		
		return builder.toString();
	}
	@Override
	public String getFullTitleNoHtml() {
		return this.title;
	}
	
	@Override
	public String toString() {
		return this.title;
	}
	@Override
	public String getHtmlTitle() {
		return this.title;
	}
	@Override
	public String getHtmlSubtitle() {
		StringBuilder builder = new StringBuilder();
		builder.append("Posted by <b>").append(this.postedByUser).append("</b>, ")
			.append(DateFlyweight.getFormatter("MMM d yyyy 'at' h:mm a").format(this.date));
		return builder.toString();
	}
}
