package org.ayizan.android.event.service;


public interface TokenGenerator {
	
	public String getClientId();
	public String getPersonId();
	public String getToken();
}
