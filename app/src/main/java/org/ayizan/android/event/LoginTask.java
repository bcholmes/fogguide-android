package org.ayizan.android.event;

import android.os.AsyncTask;
import android.util.Log;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.service.rest.LoginRequest;
import org.ayizan.android.event.service.rest.LoginResponse;
import org.ayizan.android.event.service.rest.RestResponse;
import org.ayizan.android.fogguide.service.LoginResult;

import java.io.IOException;

public class LoginTask extends AsyncTask<String, Void, RestResponse<LoginResponse>> {

	private final BaseEventApplication application;
	private String password;
	private String userid;
	private LoginResult loginResult;

	public LoginTask(BaseEventApplication wisSchedApplication, String userid, String password, LoginResult loginResult) {
		application = wisSchedApplication;
		this.userid = userid;
		this.password = password;
		this.loginResult = loginResult;
	}

	@Override
	protected RestResponse<LoginResponse> doInBackground(String... params) {
		try {
			Log.d(BaseEventApplication.TAG, "Preparing login request");
			return this.application.getRestService().login(new LoginRequest(this.userid, this.password));
		} catch (IOException e) {
			return null;
		}
	}
	@Override
	protected void onPostExecute(RestResponse<LoginResponse> result) {
		if (result != null && result.getStatusCode() == 200 && result.getBody() != null) {
			this.application.saveLoginData(result.getBody().getPersonId(), result.getBody().getApiKey(), result.getJwtToken());
			this.loginResult.loginSuccess();
			this.application.propertyChangeSupport.firePropertyChange("loginState", false, true);
		} else {
			this.loginResult.loginFailed();
		}
	}
}