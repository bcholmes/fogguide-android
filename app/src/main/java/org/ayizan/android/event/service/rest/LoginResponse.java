package org.ayizan.android.event.service.rest;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginResponse {
    String personId;
    String apiKey;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public static LoginResponse fromJson(JSONObject json) throws JSONException {
        LoginResponse response = new LoginResponse();
        response.populateFromJson(json);
        return response;
    }

    void populateFromJson(JSONObject json) throws JSONException {
        if (json.has("personId")) {
            this.personId = json.getString("personId");
        }
        if (json.has("api-key")) {
            this.apiKey = json.getString("api-key");
        }
    }
}
