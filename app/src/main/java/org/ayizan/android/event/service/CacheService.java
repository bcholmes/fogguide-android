package org.ayizan.android.event.service;

import android.content.Context;

import org.ayizan.android.event.util.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CacheService {

	private Context context;

	public CacheService(Context context) {
		this.context = context;
	}

	public CacheItem getCachedData(String key) throws IOException {
		File cacheFile = cacheFileForKey(key);

		if (cacheFile.exists()) {
			FileReader reader = new FileReader(cacheFile);
			try {
				String json = IOUtils.toString(reader);
				JSONObject jsonObject = new JSONObject(json);

				String lastModified = (jsonObject.has("lastModified") && !jsonObject.isNull("lastModified")) 
						? jsonObject.getString("lastModified") 
						: null;
				CacheItem result = new CacheItem(lastModified, (JSONObject) null);
				if (jsonObject.has("data") && !jsonObject.isNull("data")) {
					Object data = jsonObject.get("data");
					if (data instanceof JSONArray) {
						result = new CacheItem(lastModified, (JSONArray) data);
					} else if (data instanceof JSONObject) {
						result = new CacheItem(lastModified, (JSONObject) data);
					}
				}
				return result;
			} catch (JSONException e) {
				return null;
			} finally {
				IOUtils.closeQuietly(reader);
			}
		} else {
			return null;
		}
	}

	private File getUpdatesDirectory() {
		return this.context.getDir("updates", Context.MODE_PRIVATE);
	}
	
	public void writeCachedData(String key, CacheItem cacheItem) throws IOException {
		String json = cacheItem.toExternalForm();
		FileWriter writer = new FileWriter(cacheFileForKey(key));
		try {
			writer.write(json);
		} finally {
			IOUtils.closeQuietly(writer);
		}
	}

	private File cacheFileForKey(String key) {
		File cacheFile = new File(getUpdatesDirectory(), key);
		return cacheFile;
	}

	public void deleteKey(String string) {
		File file = cacheFileForKey(string);
		if (file.exists()) {
			file.delete();
		}
	}
}
