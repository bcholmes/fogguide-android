package org.ayizan.android.event.service;

import org.ayizan.android.event.model.Ranking;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SelectionResponse {

    String eventId;
    Ranking stats;

    public String getEventId() {
        return eventId;
    }

    public Ranking getStats() {
        return stats;
    }

    static SelectionResponse fromJson(JSONObject json) throws JSONException {
        SelectionResponse result = new SelectionResponse();
        if (json.has("eventId")) {
            result.eventId = json.getString("eventId");
        }
        if (json.has("stats")) {
            result.stats = Ranking.fromJson(json.getJSONObject("stats"));
        }
        return result;
    }

    public static List<SelectionResponse> fromJsonArray(JSONArray json) throws JSONException {
        List<SelectionResponse> result = new ArrayList<>();

        for (int i = 0; i < json.length(); i++) {
            JSONObject record = json.getJSONObject(i);
            result.add(fromJson(record));
        }
        return result;
    }

}
