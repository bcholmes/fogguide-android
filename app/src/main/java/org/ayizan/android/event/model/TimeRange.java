package org.ayizan.android.event.model;

import java.text.DateFormat;
import java.util.Date;

import org.ayizan.android.event.util.DateFlyweight;
import org.ayizan.android.event.util.StringUtils;

public class TimeRange implements Comparable<TimeRange>{

	private Date startTime;
	private Date endTime;

	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getEffectiveDay() {
		DateFormat formatter = DateFlyweight.getFormatter("EEEE");
		return formatter.format(isMidnightStart()
				? new Date(this.startTime.getTime() - (12L * 60L * 60L * 1000L))
				: this.startTime);
	}
	public boolean isMidnightStart() {
		return DateFlyweight.getFormatter("h:mmaa").format(this.startTime).equals("12:00AM");
	}
	public String getTimeAsString() {
		DateFormat timeFormatter = DateFlyweight.getFormatter("h:mmaa");
		StringBuilder builder = new StringBuilder();
		if (this.startTime != null) {
			builder.append(timeFormatter.format(this.startTime));
		}
		if (this.endTime != null) {
			if (builder.length() > 0) {
				builder.append("-");
			}
			builder.append(timeFormatter.format(this.endTime));
		}
		String timeString = builder.toString();
		timeString = StringUtils.replace(timeString, "12:00PM", "Noon");
		timeString = StringUtils.replace(timeString, "12:00AM", "Midnight");
		timeString = removeDuplicates(timeString, "AM");
		timeString = removeDuplicates(timeString, "PM");
		return timeString;
	}
	private String removeDuplicates(String timeString, String string) {
		if (StringUtils.countMatches(timeString, string) > 1) {
			return StringUtils.replaceOnce(timeString, string, "");
		} else {
			return timeString;
		}
	}
	public String asString() {
		DateFormat dayFormatter = DateFlyweight.getFormatter("EEEE");
		if (this.startTime != null) {
			return dayFormatter.format(this.startTime) + ", " + getTimeAsString();
		} else {
			return getTimeAsString();
		}
	}
	@Override
	public int compareTo(TimeRange another) {
		if (this.startTime.compareTo(another.startTime) == 0) {
			return this.endTime.compareTo(another.endTime);
		} else {
			return this.startTime.compareTo(another.startTime);
		}
	}

	public boolean overlapsDate(Date date) {
		if (date == null) {
			return false;
		} else {
			long millis = date.getTime();
			return getStartTime().getTime() <= millis && getEndTime().getTime() >= millis;
		}
	}

	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if (getClass() != o.getClass()) {
			return false;
		} else {
			TimeRange that = (TimeRange) o;
			return this.startTime.equals(that.startTime) && this.endTime.equals(that.endTime);
		}
	}

	public boolean isSpanningMidnight() {
		DateFormat dayFormatter = DateFlyweight.getFormatter("EEEE");
		return !dayFormatter.format(this.startTime).equals(dayFormatter.format(this.endTime));
	}
}
