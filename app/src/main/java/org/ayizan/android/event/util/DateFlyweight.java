package org.ayizan.android.event.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class DateFlyweight {

	public static TimeZone timeZone = TimeZone.getTimeZone("America/Los_Angeles");
	
	static ThreadLocal<Map<String,DateFormat>> formatterMap = new ThreadLocal<Map<String,DateFormat>>() {
		protected Map<String,DateFormat> initialValue() {
			return new HashMap<String,DateFormat>();
		}
	};
	
	public static DateFormat getIsoFormatter() {
		return getFormatter("yyyy-MM-dd'T'HH:mm:ss");
	}

	public static DateFormat getFormatter(String pattern) {
		if (!formatterMap.get().containsKey(pattern)) {
			SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.US);
			formatter.setTimeZone(getStandardTimeZone());
			formatterMap.get().put(pattern, formatter);
		}
		return formatterMap.get().get(pattern);
	}

	public static TimeZone getStandardTimeZone() {
		return timeZone;
	}
}
