package org.ayizan.android.event.service.rest;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginRequest {
    String userid;
    String password;

    public LoginRequest(String userid, String password) {
        this.userid = userid;
        this.password = password;
    }

    public String getUserid() {
        return userid;
    }

    public String getPassword() {
        return password;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("userid", this.userid);
        result.put("password", this.password);
        result.put("app-id", "dccf1a78-3a26-11e6-ac61-9e71128cae77");
        return result;
    }
}
