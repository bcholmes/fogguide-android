package org.ayizan.android.event.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.db.EventDataSource;
import org.ayizan.android.event.db.EventFinder;
import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.model.Link;
import org.ayizan.android.event.model.LinkType;
import org.ayizan.android.event.model.Links;
import org.ayizan.android.event.model.Location;
import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.model.Participation;
import org.ayizan.android.event.model.ParticipationType;
import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.model.TimeRange;
import org.ayizan.android.event.service.rest.RestResponse;
import org.ayizan.android.event.service.rest.RestService;
import org.ayizan.android.event.util.DateFlyweight;
import org.ayizan.android.event.util.IOUtils;
import org.ayizan.android.event.util.JSONResourceReader;
import org.ayizan.android.event.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.ayizan.android.event.BaseEventApplication.TAG;

public class EventService implements EventFinder {
	
	class FetchSelectionsTask extends AsyncTask<String, Void, RestResponse<List<SelectionResponse>>> {

		protected FetchSelectionsTask(Context context) {
		}


		protected RestResponse<List<SelectionResponse>> doInBackground(String... params) {
			try {
				Log.d(BaseEventApplication.TAG, "Fetching selections");
				return getRestService().fetchSelections();
			} catch (IOException e) {
				return null;
			}
		}
		@Override
		protected void onPostExecute(RestResponse<List<SelectionResponse>> result) {
			if (result != null && result.getStatusCode() == 200) {
				for (SelectionResponse response: result.getBody()) {
					if (response.getEventId() != null) {
						Event event = getEventById(response.getEventId());
						if (event != null && response.getStats() != null) {
							event.setRanking(response.getStats());
						}
					}
				}
			}
		}
	}

	class UploadSelectionsTask extends AsyncTask<String, Void, RestResponse<Void>> {

		private JSONObject json;

		UploadSelectionsTask(JSONObject json) {
			this.json = json;
		}
		@Override
		protected RestResponse<Void> doInBackground(String... strings) {
			try {
				Log.d(BaseEventApplication.TAG, "Fetching selections");
				return getRestService().uploadSelections(json, EventService.this.tokenGenerator);
			} catch (IOException e) {
				return null;
			}
		}
	}
	/*
	class UpdatesTask extends AsyncTask<URI, Void, Void> {
		
		boolean changes = false;
		
		@Override
		protected Void doInBackground(URI... urls) {
			Log.w(TAG, "Fetching event updates");

			String lastModified = null;
			File currentUpdatesCache = getUpdatesFile();
			if (currentUpdatesCache.exists()) {
				lastModified = getLastModifiedDate(currentUpdatesCache);
				Log.d(TAG, "Last modified: " + lastModified);
			}
			
			HttpClient client = new DefaultHttpClient();
			for (URI url : urls) {
				HttpGet getMethod = new HttpGet(url);
				if (lastModified != null) {
					getMethod.addHeader("If-Modified-Since", lastModified);					
				}
				try {
					PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
					String version = packageInfo.versionName;
					getMethod.addHeader("User-Agent", "FogGuide Android/" + version);
				} catch (NameNotFoundException e) {
					// skip it
				}
				try {
					HttpResponse response = client.execute(getMethod);
					if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						// save file
						Log.w(TAG, "File found");
						
						Header[] headers = response.getHeaders("Last-Modified");
						for (Header header : headers) {
							lastModified = header.getValue();
						}
						String json = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
						String jsonWithMetaData = lastModified == null 
								? "{ \"data\": " + json + "}" 
								: "{ \"lastModified\": \"" + lastModified + "\", \"data\": " + json + "}";
						writeCache(jsonWithMetaData);
						processUpdates(jsonWithMetaData);
						changes = true;
					} else if (currentUpdatesCache.exists()) {
						String jsonUpdates = getCurrentUpdates(currentUpdatesCache);
						processUpdates(jsonUpdates);
						changes = true;
					}
				} catch (ClientProtocolException e) {
					Log.e(TAG, e.getMessage());
				} catch (IOException e) {
					Log.e(TAG, e.getMessage());
				}
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (changes) {
				EventService.this.propertyChangeSupport.firePropertyChange(
						new PropertyChangeEvent(EventService.this, "events", null, EventService.this.events));
			}
		}

		private String getLastModifiedDate(File currentUpdatesCache) {
			try {
				String jsonString = getCurrentUpdates(currentUpdatesCache);
				JSONObject json = jsonString == null ? null : new JSONObject(jsonString);
				return json != null && json.has("lastModified") ? json.getString("lastModified") : null;
			} catch (JSONException e) {
				Log.e(TAG, "JSON Exception: " + e.getMessage());
				return null;
			}
		}

		private String getCurrentUpdates(File currentUpdatesCache) {
			try {
				InputStream input = new FileInputStream(currentUpdatesCache);
				try {
					return IOUtils.toString(input, "UTF-8");
				} finally {
					IOUtils.closeQuietly(input);
				}
			} catch (IOException e) {
				Log.e(TAG, "IO Exception: " + e.getMessage());
				return null;
			}
		}
	}
	*/
	
	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");

	private JSONResourceReader reader = new JSONResourceReader();
	private Map<String, Participant> participants = new HashMap<String, Participant>();
	private Map<String, Event> events = new HashMap<String, Event>();
	private Map<String, Location> locations = new HashMap<String, Location>();
	private boolean initialized = false;
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	private EventDataSource dataSource;
	private List<String> days;
	private List<Event> topLevelEvents;

	private Context context;

	private TokenGenerator tokenGenerator;

	private String currentConvention;

	public EventService(Context applicationContext, TokenGenerator tokenGenerator, String currentConvention, String databaseName) {
		this.tokenGenerator = tokenGenerator;
		this.currentConvention = currentConvention;
		initialize(applicationContext, currentConvention, databaseName);
	}
	private synchronized void initialize(Context context, String currentConvention, String databaseName) {
		this.context = context;
		this.dataSource = new EventDataSource(context, currentConvention, databaseName);
		initialize();
	}
	public void processUpdates(String jsonWithMetaData) {
		try {
			if (jsonWithMetaData != null) {
				JSONObject json = new JSONObject(jsonWithMetaData);
				if (json.has("data")) {
					JSONObject data = json.getJSONObject("data");
					
					if (data.has("event")) {
						parseAllParticipants(data.getJSONObject("event"));
						List<Event> newEvents = parseAllEvents(data.getJSONObject("event"));
						for (Event e: newEvents) {
							if (this.topLevelEvents != null && !this.topLevelEvents.contains(e)) {
								this.topLevelEvents.add(e);
							}
						}
					}
					
					Log.i(TAG, "Updates processed");
				}
			}
		} catch (JSONException e) {
			Log.e(TAG, "JSON cannot be parsed: " + e.toString());
		} catch (RuntimeException e) {
			Log.e(TAG, "Unexpected exception: " + e.toString());
		}
		
	}
	public void writeCache(String jsonWithMetaData) {
		try {
			Writer writer = new OutputStreamWriter(new FileOutputStream(getUpdatesFile()), "UTF-8");
			try {
				writer.write(jsonWithMetaData);
			} finally {
				IOUtils.closeQuietly(writer);
			}
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
	}
	private synchronized void initialize() {
		if (!this.initialized) {
			try {
				Map<String, Location> locationsById = new HashMap<>();
				JSONObject locationJson = this.reader.loadJson(this.context, "locations.json");
				parseAllLocations(locationJson.getJSONArray("locations"), locationsById);
				parseAllLocations(locationJson.getJSONArray("groups"), locationsById);
				
				JSONObject json = this.reader.loadJson(this.context, EventService.this.currentConvention + ".json");
				JSONObject eventJson = json.getJSONObject("event");
				parseAllParticipants(eventJson);
				this.topLevelEvents = parseAllEvents(eventJson);
				this.initialized = true;
				this.dataSource.readAllEvents(this);
			} catch (JSONException e) {
				Log.e(BaseEventApplication.TAG, "JSON Parse exception", e);
			}
		}
	}
	public void fetchLatestUpdates() {
		/*
		try {
			if (ConnectivityUtil.isNetworkAvailable(this.context)) {
				new UpdatesTask().execute(new URI("https://ayizan.org/fogguide/" + EventService.this.currentConvention +
						"updates.json"));
			} else {
				Log.i(TAG, "Network not available");
			}
		} catch (URISyntaxException e) {
			Log.e(TAG, "Problem fetching latest updates", e);
		}
		*/
	}
	
	private void parseAllLocations(JSONArray locations, Map<String, Location> map) throws JSONException {
		for (int i = 0, length = locations.length(); i < length; i++) {
			JSONObject locationJson = locations.getJSONObject(i);
			if (locationJson.has("name")) {
				Location location = new Location(
						locationJson.has("id") ? locationJson.getString("id") : null, 
						locationJson.getString("name"), 
						locationJson.has("thumbnail") ? locationJson.getString("thumbnail") : null,
						locationJson.has("displayName") ? locationJson.getString("displayName") : null);
				this.locations.put(location.getName(), location);
				if (location.getId() != null) {
					map.put(location.getId(), location);
				}

				if (locationJson.has("ids")) {
					List<Location> subLocations = new ArrayList<>();
					JSONArray ids = locationJson.getJSONArray("ids");
					for (int j = 0; j < ids.length(); j++) {
						String id = ids.getString(j);
						Location l = map.get(id);
						if (l != null) {
							subLocations.add(l);
						}
					}
					location.setCompositeLocations(subLocations);
				}
			}
		}
	}
	private List<Event> parseAllEvents(JSONObject eventJson) throws JSONException {
		List<Event> events = new ArrayList<>();
		JSONArray eventsArray = eventJson.has("events") ? eventJson.getJSONArray("events") : null;
		if (eventsArray != null) {
			for (int i = 0, length = eventsArray.length(); i < length; i++) {
				JSONObject event = eventsArray.getJSONObject(i);
				Event e = parseEvent(event);
				events.add(e);
				if (event.has("events")) {
					e.setSubEvents(parseAllEvents(event));
				}
			}
		}
		return events;
	}

	private Event parseEvent(JSONObject object) throws JSONException {
		String id = object.has("id") ? object.getString("id") : null;
		if (id == null) {
			id = object.has("databaseId") ? object.getString("databaseId") : null;
		}
		Event event = this.events.get(id);
		if (event == null) {
			event = new Event(id);
			this.events.put(id, event);
		}
		if (object.has("title")) {
			event.setTitle(object.getString("title"));
		}
		if (object.has("externalId")) {
			event.setExternalId(object.getString("externalId"));
		}
		if (object.has("description")) {
			event.setDescription(object.has("description") ? object.getString("description") : null);
		}
		if (object.has("location")) {
			event.setLocation(resolveLocation(object.getString("location")));
		} else if (event.getLocation() == null) {
			event.setLocation(new Location(null, "", null, null));
		}
		if (object.has("hashTag")) {
			event.setHashTag(object.getString("hashTag"));
		}
		if (object.has("sortKey")) {
			event.setSortKey(object.getString("sortKey"));
		}
		if (object.has("type")) {
			event.setType(object.getString("type"));
		}
		if (object.has("track")) {
			event.setTrack(object.getString("track"));
		}
		if (object.has("private")) {
			event.setPrivate(object.getBoolean("private"));
		}
		if (object.has("alternate")) {
			event.setAlternate(object.getBoolean("alternate"));
		}
		if (object.has("cancelled")) {
			event.setCancelled(object.getBoolean("cancelled"));
		}

		if (object.has("startTime") && object.has("endTime")) {
			TimeRange range = new TimeRange();
			try {
				range.setStartTime(this.formatter.parse(object.getString("startTime")));
				range.setEndTime(this.formatter.parse(object.getString("endTime")));
			} catch (ParseException e) {
				Log.e(BaseEventApplication.TAG, "Problem parsing date for event " + id, e);
			}
			event.setTimeRange(range);
		}

		populateLinks(object, event);

		handleParticipation(object, event);
		return event;
	}

	private void populateLinks(JSONObject object, Links links) throws JSONException {
		if (object.has("links")) {
			JSONArray linksList = object.getJSONArray("links");
			for (int j = 0; j < linksList.length(); j++) {
				Link link = new Link();
				JSONObject linkJson = linksList.getJSONObject(j);
				if (linkJson.has("type")) {
					try {
						link.setType(LinkType.valueOf(linkJson.getString("type")));
					} catch (IllegalArgumentException e) {

					}
				}
				if (linkJson.has("name")) {
					link.setName(linkJson.getString("name"));
				}
				if (linkJson.has("href")) {
					link.setHref(linkJson.getString("href"));
				}
				links.getLinks().add(link);
			}
		}
	}

	Location resolveLocation(String name) {
		if (!this.locations.containsKey(name)) {
			Log.i(TAG, "Location " + name + " cannot be found. Creating one");
			this.locations.put(name, new Location(null, name, null, null));
		}
		return this.locations.get(name);
	}
	private void handleParticipation(JSONObject object, Event event)
			throws JSONException {
		if (object.has("participants")) {
			JSONArray participantsArray = object.getJSONArray("participants");
			for (int i = 0, length = participantsArray.length(); i < length; i++) {
				JSONObject json = participantsArray.getJSONObject(i);
				String participantId = json.getString("id");
				Participant participant = this.participants.get(participantId);
				
				Participation participation = event.getParticipation(participant);
				if (participation == null) {
					participation = new Participation();
					participation.setParticipant(participant);
					event.getParticipants().add(participation);
				}
				if (json.has("hidden")) {
					boolean hidden = json.getBoolean("hidden");
					participation.setHidden(hidden);
				}
				if (json.has("cancelled")) {
					boolean cancelled = json.getBoolean("cancelled");
					participation.setCancelled(cancelled);
				}
				if (json.has("type")) {
					if (ParticipationType.MODERATOR.name().equalsIgnoreCase(json.getString("type"))) {
						participation.setType(ParticipationType.MODERATOR);
					} else {
						participation.setType(ParticipationType.PANELIST);
					}
				}
				if (!participant.getEvents().contains(event)) {
					participant.getEvents().add(event);
				}
			}
		}
	}

	private void parseAllParticipants(JSONObject json) throws JSONException {
		JSONArray jsonArray = json.getJSONArray("participants");
		if (jsonArray != null) {
			for (int i = 0, length = jsonArray.length(); i < length; i++) {
				JSONObject object = jsonArray.getJSONObject(i);
				String id = object.getString("id");
				Participant participant = this.participants.get(id);
				if (participant == null) {
					JSONObject name = object.getJSONObject("name");
					participant = new Participant(id,
							name.getString("display"), name.getString("sort"),
							object.has("bio") ?  object.getString("bio") : null,
							object.has("pronouns") ?  object.getString("pronouns") : null);
					this.participants.put(id, participant);
				}
				if (object.has("name")) {
					JSONObject name = object.getJSONObject("name");
					if (name.has("display")) {
						participant.setName(name.getString("display"));
					}
					if (name.has("sort")) {
						participant.setSortName(name.getString("sort"));
					}
				}
				populateLinks(object, participant);

				if (object.has("cancelled")) {
					participant.setCancelled(object.getBoolean("cancelled"));
				}
			}
		}
	}
	
	public List<Participant> getParticipants() {
		if (!this.initialized) {
			throw new IllegalStateException("Not initialized");
		}
		List<Participant> result = new ArrayList<Participant>();
		for (Participant participant : this.participants.values()) {
			if (participant.isVisible()) {
				result.add(participant);
			}
		}
		Collections.sort(result);
		return result;
	}
	
	public String[] getProgramItems() {
		return new String[] { "First", "Second", "Third" };
	}
	
	public Renderable getEntityById(EntityType type, String id) {
		if (type == EntityType.PARTICIPANT) {
			return getParticipantById(id);
		} else {
			return getEventById(id);
		}
	}

	public Event getEventById(String id) {
		if (!this.initialized) {
			throw new IllegalStateException("Not initialized");
		}
		return this.events.get(id);
	}

	public Participant getParticipantById(String participantId) {
		if (!this.initialized) {
			throw new IllegalStateException("Not initialized");
		}
		return this.participants.get(participantId);
	}

	public List<Event> getLeafProgramItems() {
		if (!this.initialized) {
			throw new IllegalStateException("Not initialized");
		}
		ArrayList<Event> list = new ArrayList<Event>();
		for (Event event : this.events.values()) {
			if (event.isVisible() && (event.getSubEvents() == null || event.getSubEvents().isEmpty())) {
				list.add(event);
			}
		}
		Collections.sort(list);
		return list;
	}

	public List<Event> getTopLevelProgramItems() {
		if (!this.initialized) {
			throw new IllegalStateException("Not initialized");
		}
		ArrayList<Event> list = new ArrayList<Event>();
		for (Event event : this.topLevelEvents) {
			if (event.isVisible()) {
				list.add(event);
			}
		}
		Collections.sort(list);
		return list;
	}

	public void saveEvent(Event event) {
		this.dataSource.saveEvent(event);
		event.setTouched(true);
		this.propertyChangeSupport.firePropertyChange(new PropertyChangeEvent(event, "event", null, event));
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}
	private File getUpdatesFile() {
		File cacheDir = this.context.getCacheDir();
		File currentUpdatesCache = new File(cacheDir, EventService.this.currentConvention + "updates.json");
		return currentUpdatesCache;
	}
	public void saveEvents(List<Event> events) {
		if (events != null) {
			for (Event event : events) {
				this.dataSource.saveEvent(event);
				event.setTouched(true);
			}
			this.propertyChangeSupport.firePropertyChange(
					new PropertyChangeEvent(this, "events", null, this.events));
		}
	}
	
	public void fetchLatestRankings() {
		new FetchSelectionsTask(this.context).execute();
	}
	
	public void uploadSelections() {
		try {
			JSONObject json = getSelectionsJson();
			if (json != null) {
				new UploadSelectionsTask(json).execute();
			} else {
				Log.d(BaseEventApplication.TAG, "No selections to upload");
			}
		} catch (JSONException e) {
			Log.e(BaseEventApplication.TAG, "JSON error", e);
		}
	}

	private RestService getRestService() {
		BaseEventApplication application = (BaseEventApplication) context.getApplicationContext();
		return application.getRestService();
	}

	private JSONObject getSelectionsJson() throws JSONException {
		JSONObject object = new JSONObject();
		object.put("clientId", this.tokenGenerator.getClientId());
		if (StringUtils.isNotEmpty(this.tokenGenerator.getPersonId())) {
			object.put("personId", this.tokenGenerator.getPersonId());
		}
		object.put("convention", this.currentConvention);
		JSONArray selections = new JSONArray();
		
		for (Event event : getLeafProgramItems()) {
			if (event.isTouched()) {
				JSONObject temp = new JSONObject();
				temp.put("eventId", event.getId());
				temp.put("scheduled", event.isInSchedule());
				temp.put("highlghted", event.isHighlighted());
				temp.put("yay", event.isYay());
				temp.put("omg", event.isOmg());
				selections.put(temp);
			}
		}
		
		object.put("selections", selections);
		
		return selections.length() > 0 ? object : null;
	}

	public TimeRange getClosestTimeRangeToNow() {
		List<Event> events = getLeafProgramItems();
		Date now = new Date();
		String today = DateFlyweight.getFormatter("EEEE").format(now);
		TimeRange result = null;
		Date pseudoNow = null;

		for (Event event: events) {

			if (today.equals(event.getTimeRange().getEffectiveDay())) {
				if (pseudoNow == null) {
					pseudoNow = createPseudoDate(now, event.getTimeRange().getStartTime());
				}

				if (result == null) {
					result = event.getTimeRange();
				}

				if (pseudoNow.before(event.getTimeRange().getStartTime())) {
					if (!result.overlapsDate(pseudoNow)) {
						result = event.getTimeRange();
					}
					break;
				} else  if (event.getTimeRange().overlapsDate(pseudoNow) && !result.overlapsDate(pseudoNow)) {
					result = event.getTimeRange();
				}
			}
		}
		return result;
	}

	private Date createPseudoDate(Date baseDate, Date dayOfMonthPart) {
		String date = DateFlyweight.getFormatter("yyyy-MM-dd").format(dayOfMonthPart);
		String time = DateFlyweight.getFormatter("HH:mm:ss").format(baseDate);
		try {
			return DateFlyweight.getFormatter("yyyy-MM-dd HH:mm:ss").parse(date + " " + time);
		} catch (ParseException e) {
			Log.w(TAG, "Could not parse date \"" + date + " " + time + "\"");
			return baseDate;
		}
	}

	public List<Location> getAllLocations() {
		List<Location> result = new ArrayList<>(this.locations.values());
		Collections.sort(result);
		return result;
	}

	public List<String> getDays() {
		if (this.days == null) {
			List<String> temp = new ArrayList<>();
			for (Event event : getLeafProgramItems()) {
				String day = event.getTimeRange().getEffectiveDay();
				if (!temp.contains(day)) {
					temp.add(day);
				}
			}
			this.days = temp;
		}
		return this.days;
	}
}
