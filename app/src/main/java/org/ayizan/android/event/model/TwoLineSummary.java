package org.ayizan.android.event.model;

public interface TwoLineSummary {

	public String getHtmlTitle();
	public String getHtmlSubtitle();
}
