package org.ayizan.android.event;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import org.ayizan.android.event.service.EventService;
import org.ayizan.android.event.service.GridViewService;
import org.ayizan.android.event.service.ImageService;
import org.ayizan.android.event.service.TokenGenerator;
import org.ayizan.android.event.service.rest.RestService;
import org.ayizan.android.event.util.PersonIdProvider;

import java.beans.PropertyChangeSupport;

/**
 * Created by bcholmes on 2018-02-19.
 */

public abstract class BaseEventApplication extends Application implements PersonIdProvider, TokenGenerator, JwtTokenProvider {

    public static String TAG = "";
    String currentConvention;
    String databaseName;
    protected EventService eventService;
    private RestService restService;

    protected GridViewService gridViewService;

    PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public BaseEventApplication(String tag, String currentConvention, String databaseName) {
        TAG = tag;
        this.currentConvention = currentConvention;
        this.databaseName = databaseName;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.eventService = new EventService(getApplicationContext(), this, this.currentConvention, this.databaseName);
        this.gridViewService = new GridViewService(getApplicationContext(), this.eventService);
        this.restService = new RestService(this, currentConvention, this);

    }

    public EventService getEventService() {
        return eventService;
    }

    public GridViewService getGridViewService() {
        return gridViewService;
    }

    public abstract ImageService getImageService();

    public void saveLoginData(String personId, String appKey, String jwtToken) {
        SharedPreferences sharedPref = getApplicationSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("personId", personId);
        editor.putString("appKey", appKey);
        editor.putString("jwtToken", jwtToken);
        editor.apply();
    }

    public String getJwtToken() {
        return getApplicationSharedPreferences().getString("jwtToken", null);
    }

    private SharedPreferences getApplicationSharedPreferences() {
        return getSharedPreferences("FogGuide", Context.MODE_PRIVATE);
    }
    public RestService getRestService() {
        return restService;
    }

}
