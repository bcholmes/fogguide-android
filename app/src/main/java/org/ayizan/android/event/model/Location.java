package org.ayizan.android.event.model;

import androidx.annotation.NonNull;

import org.ayizan.android.event.util.StringUtils;

import java.util.Arrays;
import java.util.List;

public class Location implements Comparable<Location> {

	private String id;
	private String name;
	private String thumbnail;
	private String displayName;
	private List<Location> compositeLocations;
	
	public Location(String id, String name, String thumbnail, String displayName) {
		this.id = id;
		this.name = name;
		this.thumbnail = thumbnail;
		this.displayName = displayName;
	}
	public String getName() {
		return this.name;
	}
	public String getThumbnail() {
		return this.thumbnail;
	}
	public String getId() {
		return this.id;
	}
	public String getDisplayName() {
		return StringUtils.isBlank(this.displayName) ? getName() : this.displayName;
	}
	public int getSortKey() {
		try {
			return this.id == null ? 0 : Integer.parseInt(this.id);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	@Override
	public int compareTo(@NonNull Location location) {
		if (this.getSortKey() == location.getSortKey()) {
			return this.name.compareTo(location.name);
		} else {
			return this.getSortKey() - location.getSortKey();
		}
	}

	public boolean isGroup() {
		return this.compositeLocations != null && !this.compositeLocations.isEmpty();
	}

	public List<Location> getAllLocations() {
		if (isGroup()) {
			return this.compositeLocations;
		} else {
			return Arrays.asList(this);
		}
	}

	public List<Location> getCompositeLocations() {
		return compositeLocations;
	}

	public void setCompositeLocations(List<Location> compositeLocations) {
		this.compositeLocations = compositeLocations;
	}
}
