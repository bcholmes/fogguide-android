package org.ayizan.android.event.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.util.ConnectivityUtil;
import org.ayizan.android.event.util.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.ayizan.android.event.BaseEventApplication.TAG;

public class ImageService {

	/*
	public class UploadAvatarTask extends HttpRequestTask {

		private JSONObject json;

		public UploadAvatarTask(JSONObject json) {
			super(ImageService.this.context);
			this.json = json;
		}
		
		@Override
		protected void addCommonHeaders(HttpRequestBase postMethod) {
			super.addCommonHeaders(postMethod);
			postMethod.addHeader("Authorization", "Basic " + ImageService.this.tokenGenerator.getToken());
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				Log.d(TAG, "Preparing avatar upload request");
				HttpPost postMethod = new HttpPost("https://ayizan.org/api/avatar");
				postMethod.setHeader("Content-Type", "application/json");
				postMethod.setHeader("Accept", "application/json");
				addCommonHeaders(postMethod);
				String jsonString = this.json.toString();
				
				postMethod.setEntity(new ByteArrayEntity(jsonString.getBytes("UTF-8")));
		        
		        HttpResponse response = this.client.execute(postMethod);

		        Log.i(TAG, "HTTP Avatar upload status " + response.getStatusLine().getStatusCode());
		        return response.getStatusLine().getStatusCode() == HttpStatus.SC_OK;
			} catch (IOException e) {
				Log.e(TAG, "error sending request", e);
				return false;
			}
		}
		
		protected void onPostExecute(Boolean result) {
			if (result == Boolean.TRUE) {
				deleteUploadCache();
			}
		}
	}
	*/

	Context context;
	private String conventionId;
	private TokenGenerator tokenGenerator;
	private CacheService cacheService;

	public ImageService(Context context, TokenGenerator tokenGenerator, String conventionId) {
		this.context = context;
		this.tokenGenerator = tokenGenerator;
		this.conventionId = conventionId;
		this.cacheService = new CacheService(context);
	}
	
	public void saveAvatar(String personId, byte[] byteArray) {
		
		File avatar = fileForPersonId(personId);
		
		try {
			FileOutputStream outputStream = new FileOutputStream(avatar);
			try {
				outputStream.write(byteArray);
			} finally {
				IOUtils.closeQuietly(outputStream);
			}
		} catch (IOException e) {
			Log.e(TAG, "Problem saving avatar", e);
		}
	}

	public File fileForParticipant(Participant participant) {
		return fileForPersonId(participant.getId());
	}
	private File fileForPersonId(String personId) {
		File dir = this.context.getDir("avatars", Context.MODE_PRIVATE);
		return new File(dir, this.conventionId + "_person" + personId);
	}

	public boolean isAvatarAvailable(Participant participant) {
		return fileForPersonId(participant.getId()).exists();
	}
	
	public String getAvatarAsEncodedHtmlImage(Participant participant) {
		File avatar = fileForPersonId(participant.getId());
		try {
			byte[] imageData = toByteArray(avatar);
			return "data:" + getDataType(imageData) + ";base64," + Base64.encodeToString(imageData, Base64.DEFAULT);
		} catch (IOException e) {
			return "";
		}
	}

	private String getDataType(byte[] imageData) {
		if (imageData == null || imageData.length == 0) {
			return null;
		} else {
			switch ((int) imageData[0]) {
			case 0xff:
				return "image/jpeg";
			case 0x89:
				return "image/png";
			case 0x47:
				return "image/gif";
			case 0x49:
			case 0x4D:
				return "image/tiff";
			default:
				return null;
			}
		}
	}

	private byte[] toByteArray(File avatar) throws IOException {
		FileInputStream input = new FileInputStream(avatar);
		try {
			return IOUtils.toByteArray(input);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	public void writeImageToFileSystem(Bitmap bitmap, Participant participant) throws IOException {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 90, bytes);
		byte[] data = bytes.toByteArray();
		
		File file = fileForPersonId(participant.getId());
		FileOutputStream output = new FileOutputStream(file);
		try {
			output.write(data);
		} finally {
			IOUtils.closeQuietly(output);
		}
		prepareForUpload(data);
	}

	private void prepareForUpload(byte[] bytes) {
		try {
			JSONObject json = createUploadJson(bytes);
			this.cacheService.writeCachedData("avatarUpload", new CacheItem(null, json));
			upload(json);
		} catch (JSONException e) {
			Log.e(TAG, "Unable to create the avatar upload JSON", e);
		} catch (IOException e) {
			Log.e(TAG, "Unable to cache the avatar", e);
		}
	}

	public void uploadQueuedImage() {
		try {
			CacheItem item = this.cacheService.getCachedData("avatarUpload");
			if (item != null && item.getJson() != null) {
				upload(item.getJson());
			} else {
				Log.i(TAG, "No avatar to upload");
			}
		} catch (IOException e) {
			Log.e(TAG, "Problem with image cache", e);
		}
	}
	
	private void upload(JSONObject json) {
		if (ConnectivityUtil.isNetworkAvailable(this.context)) {
			/*
			new UploadAvatarTask(json).execute();
			*/
		}
	}
	
	void deleteUploadCache() {
		this.cacheService.deleteKey("avatarUpload");
	}

	private JSONObject createUploadJson(byte[] bytes) throws JSONException {
		JSONObject result = new JSONObject();
		result.put("clientId", this.tokenGenerator.getClientId());
		result.put("personId", this.tokenGenerator.getPersonId());
		result.put("mimeType", "image/png");
		result.put("convention", this.conventionId);
		
		Log.d(TAG, result.toString());
		result.put("photo", Base64.encodeToString(bytes, Base64.NO_WRAP));
		return result;
	}
}
