package org.ayizan.android.event.model;

public enum ParticipationType {

	MODERATOR, PANELIST;

	public boolean isModerator() {
		return this == MODERATOR;
	}
}
