package org.ayizan.android.event.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION = 3;

	public DbHelper(Context context, String databaseName) {
		super(context, databaseName, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE event (event_id int, is_highlighted char(1), " +
				"my_schedule char(1), rating_overall int, rating_on_topic int, " +
				"convention varchar(255), yay char(1) not null default 'N', "
				+ "omg char(1) not null default 'N', rank_score int not null default 0,"
				+ "yay_score int not null default 0, omg_score int not null default 0," +
				"primary key (event_id))");

	}

	public void onRevision002(SQLiteDatabase db) {
		db.execSQL("ALTER TABLE event ADD COLUMN convention varchar(255)");

	}

	public void onRevision003(SQLiteDatabase db) {
		db.execSQL("ALTER TABLE event ADD COLUMN yay char(1) not null default 'N'");
		db.execSQL("ALTER TABLE event ADD COLUMN omg char(1) not null default 'N'");
		db.execSQL("ALTER TABLE event ADD COLUMN rank_score int not null default 0");
		db.execSQL("ALTER TABLE event ADD COLUMN yay_score int not null default 0");
		db.execSQL("ALTER TABLE event ADD COLUMN omg_score int not null default 0");
		
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		for (int i = oldVersion+1; i <= newVersion; i++) {
			Log.w(getClass().getName(),
			        "Upgrading database from version " + (i-1) + " to "
			            + i);
			switch (i) {
			case 3:
				onRevision003(db);
				break;
			case 2:
				onRevision002(db);
			default:
			}
		}
	}
}
