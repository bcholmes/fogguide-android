package org.ayizan.android.event.service.rest;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import android.util.Log;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.JwtTokenProvider;
import org.ayizan.android.event.service.SelectionResponse;
import org.ayizan.android.event.service.TokenGenerator;
import org.ayizan.android.event.util.ApplicationNameUtil;
import org.ayizan.android.event.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static org.ayizan.android.event.BaseEventApplication.TAG;

/**
 * Created by bcholmes on 2017-08-30.
 */
public class RestService {

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    private Context context;
    String conventionId;
    private JwtTokenProvider tokenProvider;

    protected String getUserAgent() {
        try {
            PackageInfo packageInfo = this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0);
            String version = packageInfo.versionName;
            return ApplicationNameUtil.getApplicationName() + " Android/" + version;
        } catch (PackageManager.NameNotFoundException e) {
            return ApplicationNameUtil.getApplicationName() + " Android/";
        }
    }

    @NonNull
    private String getUserAgentDevice() {
        return android.os.Build.DEVICE + " " + android.os.Build.MODEL + " " + android.os.Build.PRODUCT;
    }


    public RestService(Context context, String conventionId, JwtTokenProvider tokenProvider) {
        this.context = context;
        this.conventionId = conventionId;
        this.tokenProvider = tokenProvider;
    }

    public FetchAvatarListResponse fetchAvatars() throws IOException {

        try {
            Request request = new Request.Builder()
                    .url("https://ayizan.org/api/avatar/" + ApplicationNameUtil.getApplicationName().toLowerCase() + "/")
                    .header("X-UA-Device", getUserAgentDevice())
                    .header("User-Agent", getUserAgent())
                    .build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                JSONObject json = new JSONObject(response.body().string());
                return FetchAvatarListResponse.fromJson(json);
            } else {
                return null;
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem with JSON", e);
            return null;
        }
    }

    /*
    public FetchAnnouncementsResponse fetchAnnouncements() throws IOException {

        try {
            Request request = new Request.Builder()
                    .url("https://wiscon.net/wp-json/posts")
                    .header("X-UA-Device", getUserAgentDevice())
                    .header("User-Agent", getUserAgent())
                    .build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                JSONArray json = new JSONArray(response.body().string());
                return FetchAnnouncementsResponse.fromJson(json);
            } else {
                return null;
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem with JSON", e);
            return null;
        }
    }
    */

    public RestResponse<String> fetchUpdates(URL updatesUrl, String lastModified) throws IOException {

        Request.Builder builder = new Request.Builder()
                .url(updatesUrl)
                .header("X-UA-Device", getUserAgentDevice())
                .header("User-Agent", getUserAgent());
        if (StringUtils.isNotEmpty(lastModified)) {
            builder.header("If-Modified-Since", lastModified);
        }
        Request request = builder.build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            String data = response.body().string();
            return RestResponse.create(response, data);
        } else {
            return RestResponse.createError(response);
        }
    }

    public RestResponse<List<SelectionResponse>> fetchSelections() throws IOException {

        try {
            Request request = new Request.Builder()
                    .url("https://ayizan.org/api/selections/" + this.conventionId + "/")
                    .header("X-UA-Device", getUserAgentDevice())
                    .header("User-Agent", getUserAgent())
                    .build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                JSONArray json = new JSONArray(response.body().string());
                return RestResponse.create(response, SelectionResponse.fromJsonArray(json));
            } else {
                return RestResponse.createError(response);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem with JSON", e);
            return null;
        }
    }

    public RestResponse<LoginResponse> login(LoginRequest restRequest) throws IOException {
        try {
            Request request = new Request.Builder()
                    .url("https://ayizan.org/api/login")
                    .header("X-UA-Device", getUserAgentDevice())
                    .header("User-Agent", getUserAgent())
                    .post(RequestBody.create(MediaType.parse("application/json"), restRequest.toJson().toString()))
                    .build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                JSONObject json = new JSONObject(response.body().string());
                return RestResponse.create(response, LoginResponse.fromJson(json));
            } else {
                Log.i(BaseEventApplication.TAG, "Login unsuccessful: " + (response.body() != null ? response.body().string() : "no data"));
                return RestResponse.createError(response);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Problem with JSON", e);
            return RestResponse.createJsonError();
        }
    }

    public RestResponse<Void> uploadSelections(JSONObject json, TokenGenerator tokenGenerator) throws IOException {
        Request request = new Request.Builder()
                .url("https://ayizan.org/api/selections")
                .header("X-UA-Device", getUserAgentDevice())
                .header("User-Agent", getUserAgent())
                .post(RequestBody.create(MediaType.parse("application/json"), json.toString()))
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            return RestResponse.create(response, null);
        } else {
            Log.i(BaseEventApplication.TAG, "Login unsuccessful: " + (response.body() != null ? response.body().string() : "no data"));
            return RestResponse.createError(response);
        }
    }

    public RestResponse<AssignmentsResponse> fetchAssignments() throws IOException {
        if (this.tokenProvider.isLoggedIn()) {
            try {
                Request request = new Request.Builder()
                        .url("https://ayizan.org/api/assignments")
                        .header("X-UA-Device", getUserAgentDevice())
                        .header("User-Agent", getUserAgent())
                        .header("Authorization", "Bearer " + this.tokenProvider.getJwtToken())
                        .post(RequestBody.create(MediaType.parse("application/json"), "{}"))
                        .build();
                Response response = client.newCall(request).execute();
                if (response.isSuccessful()) {
                    JSONObject json = new JSONObject(response.body().string());
                    return RestResponse.create(response, AssignmentsResponse.fromJson(json));
                } else {
                    Log.i(BaseEventApplication.TAG, "Login unsuccessful: " + (response.body() != null ? response.body().string() : "no data"));
                    return RestResponse.createError(response);
                }
            } catch (JSONException e) {
                Log.e(TAG, "Problem with JSON", e);
                return RestResponse.createJsonError();
            }
        } else {
            return RestResponse.createAuthenticationError();
        }
    }


    public RestResponse<Void> uploadAvatar(JSONObject requestJson) throws IOException {
        if (this.tokenProvider.isLoggedIn()) {
            Request request = new Request.Builder()
                    .url("https://ayizan.org/api/avatar")
                    .header("X-UA-Device", getUserAgentDevice())
                    .header("User-Agent", getUserAgent())
                    .header("Authorization", "Bearer " + this.tokenProvider.getJwtToken())
                    .post(RequestBody.create(MediaType.parse("application/json"), requestJson.toString()))
                    .build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return RestResponse.create(response, null);
            } else {
                Log.i(BaseEventApplication.TAG, "Login unsuccessful: " + (response.body() != null ? response.body().string() : "no data"));
                return RestResponse.createError(response);
            }
        } else {
            return RestResponse.createAuthenticationError();
        }
    }


    public RestResponse<byte[]> fetchAvatar(FetchAvatarListResponse.AvatarRecord record) throws IOException {
        Request request = new Request.Builder()
                .url(getAvatarUrl(record.getAvatarId()))
                .header("X-UA-Device", getUserAgentDevice())
                .header("User-Agent", getUserAgent())
                .get()
                .build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()) {
            return RestResponse.create(response, response.body().bytes());
        } else {
            Log.i(BaseEventApplication.TAG, "Avatar fetch failed: " + (response.body() != null ? response.body().string() : "no data"));
            return RestResponse.createError(response);
        }
    }


    private String getAvatarUrl(String personId) {
        return "https://ayizan.org/api/avatarById/" + ApplicationNameUtil.getApplicationName().toLowerCase() + "/" + personId;
    }

}
