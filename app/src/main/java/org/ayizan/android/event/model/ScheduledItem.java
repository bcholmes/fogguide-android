package org.ayizan.android.event.model;

public interface ScheduledItem {

    String getFullIdentifier();
    TimeRange getTimeRange();
    String getFullTitleNoHtml();
    Location getLocation();
}
