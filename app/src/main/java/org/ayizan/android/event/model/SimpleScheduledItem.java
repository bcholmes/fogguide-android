package org.ayizan.android.event.model;

public class SimpleScheduledItem implements ScheduledItem {

    String id;
    String description;
    String href;
    TimeRange timeRange;
    Location location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public String getFullIdentifier() {
        return "Simple#" + this.id;
    }

    @Override
    public TimeRange getTimeRange() {
        return this.timeRange;
    }

    @Override
    public String getFullTitleNoHtml() {
        return this.description;
    }

    public void setTimeRange(TimeRange timeRange) {
        this.timeRange = timeRange;
    }

    @Override
    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
