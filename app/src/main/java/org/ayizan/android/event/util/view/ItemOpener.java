package org.ayizan.android.event.util.view;

/**
 * Created by BC Holmes on 2016-08-16.
 */
public interface ItemOpener<T> {

    public void open(T item);

}
