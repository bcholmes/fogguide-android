package org.ayizan.android.event.service.rest;

import static org.ayizan.android.event.BaseEventApplication.TAG;

import android.util.Log;

import org.ayizan.android.event.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bcholmes on 2017-08-31.
 */

public class FetchAvatarListResponse {

    public static class AvatarRecord {

        private String personId;
        private String avatarId;

        public AvatarRecord(String personId, String avatarId) {
            this.personId = personId;
            this.avatarId = avatarId;
        }

        public String getPersonId() {
            return personId;
        }

        public void setPersonId(String personId) {
            this.personId = personId;
        }

        public String getAvatarId() {
            return avatarId;
        }

        public void setAvatarId(String avatarId) {
            this.avatarId = avatarId;
        }
    }


    public List<AvatarRecord> getRecords() {
        return records;
    }

    List<AvatarRecord> records;

    static FetchAvatarListResponse fromJson(JSONObject json) throws JSONException {
        FetchAvatarListResponse response = new FetchAvatarListResponse();
        response.records = convertToAvatarRecords(json);
        return response;
    }


    static List<AvatarRecord> convertToAvatarRecords(JSONObject json) throws JSONException {
        JSONArray array = json.has("records") ? json.getJSONArray("records") : null;
        return array == null ? null : convertToAvatarRecords(array);
    }

    static List<AvatarRecord> convertToAvatarRecords(JSONArray json) throws JSONException{
        List<AvatarRecord> result = new ArrayList<>();
        for (int i = 0; i < json.length(); i++) {
            JSONObject object = json.getJSONObject(i);
            String personId = object.has("id") ? object.getString("id") : null;
            String id = object.has("avatarId") ? object.getString("avatarId") : null;
            if (StringUtils.isNotEmpty(id)) {
                Log.i(TAG, "avatar for " + id);
                result.add(new AvatarRecord(personId, id));
            }
        }
        return result;
    }
}
