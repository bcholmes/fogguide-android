package org.ayizan.android.event.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by bcholmes on 2018-03-14.
 */
public class Link {
    private LinkType type;
    private String name;
    private String href;

    public LinkType getType() {
        return type;
    }

    public void setType(LinkType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public int hashCode() {
        return new HashCodeBuilder().append(this.type).append(this.name).append(this.href).toHashCode();
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        } else if (o == this) {
            return true;
        } else if (o.getClass() != getClass()) {
            return false;
        } else {
            Link that = (Link) o;
            return new EqualsBuilder().append(this.type, that.type).append(this.name, that.name).append(this.href, that.href).isEquals();
        }
    }
}
