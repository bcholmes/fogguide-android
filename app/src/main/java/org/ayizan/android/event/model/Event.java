package org.ayizan.android.event.model;

import org.ayizan.android.event.util.PersonIdProvider;
import org.ayizan.android.event.util.RenderContext;
import org.ayizan.android.event.util.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class Event implements Renderable, SortableRecord<Event>, Links, ScheduledItem {

	private String id;
	private String title;
	private String externalId;
	private String description;
	private Location location;
	private String hashTag;
	private String sortKey;
	private String type;
	private String track;
	private TimeRange timeRange;
	private List<Participation> participants = new ArrayList<Participation>();
	private boolean highlighted;
	private boolean isInSchedule;
	private boolean isPrivate;
	private boolean alternate;

	private boolean yay;
	private boolean omg;
	private Ranking ranking = new Ranking();
	private boolean touched;
	private List<Event> subEvents;
	private boolean cancelled;
	private Set<Link> links = new LinkedHashSet<>();

	public Event(String id) {
		this.id = id;
	}
	public Event() {
	}

	public String getFullIdentifier() {
		return "Event#" + getId();
	}
	@Override
	public String getGroup() {
		return this.timeRange.asString();
	}

	public String getTrackCategory() {
		if (!StringUtils.isBlank(this.track)) {
			return this.track;
		} else if (!StringUtils.isBlank(this.type) && !this.type.equals("Event")) {
			return this.type;
		} else {
			return null;
		}
	}

	public boolean isMyEvent(PersonIdProvider personIdProvider) {
		String id = personIdProvider.getPersonId();
		if (StringUtils.isEmpty(id)) {
			return false;
		} else {
			boolean found = false;
			for (Participation participation : this.participants) {
				if (id.equals(participation.getParticipant().getId())) {
					found = true;
					break;
				}
			}
			return found;
		}
	}

	@Override
	public String getFullHtml(RenderContext renderContext) {
		StringBuilder builder = new StringBuilder();
		builder.append("<html><head><title>Bio</title><link rel=\"stylesheet\" type=\"text/css\" href=\"./list.css\" />")
				.append("</head><body>").append("<div class=\"content\"><div class=\"main")
				.append(this.highlighted ? " yellow" : "").append("\">")
				.append("<h2>").append(getFullTitle()).append("</h2>");
		if (this.location.getThumbnail() != null) {
			builder.append("<div class=\"thumbnail\"><img src=\"./" + this.location.getThumbnail() +
					"\" /></div>");
		}

		if (!StringUtils.isBlank(getTrackCategory())) {
			builder.append("<p class=\"subtitle\">").append(getTrackCategory()).append("<br />")
					.append(getSubTitle()).append("</p>");
		} else {
			builder.append("<p class=\"subtitle\">").append(getSubTitle()).append("</p>");
		}

		if (!StringUtils.isBlank(this.description)) {
			builder.append(this.description);
		}

		if (isMyEvent(renderContext)) {
			builder.append("<p class=\"notice\"><i>You are a participant on this panel.</i></p>");
		}
		if (!StringUtils.isBlank(this.hashTag)) {
			builder.append("<p class=\"hashTag\">").append(this.hashTag)
					.append("<a href=\"/tweet/\" class=\"action-button floatRight\"><img src=\"./Twitter-icon-90.png\" width=\"45\" height=\"45\"/></a>")
					.append("</p>");
		}

		if (this.ranking != null && (this.ranking.getYayCount() > 0 || this.ranking.getOmgCount() > 0)) {
			builder.append("<p class=\"event-rank\">");
			if (this.ranking.getYayCount() > 0) {
				builder.append("\\o/").append(this.ranking.getYayCount());
				if (this.ranking.getOmgCount() > 0) {
					builder.append(", ");
				}
			}
			if (this.ranking.getOmgCount() > 0) {
				builder.append("D:").append(this.ranking.getOmgCount());
			}
			builder.append("</p>");
		}

		builder.append("<div class=\"clear\"></div></div>");
		if (this.subEvents != null && !this.subEvents.isEmpty()) {
			builder.append("<ul class=\"nav\">");
			for (Event event : this.subEvents) {
				builder.append("<li><a href=\"http://device/event/")
						.append(event.getId()).append("\"><span");
				builder.append(">");
				builder.append(event.getFullTitle()).append("</span></a></li>");
			}
			builder.append("</ul>");
		}

		if (!this.participants.isEmpty()) {
			builder.append("<ul class=\"nav\">");
			for (Participation participation : this.participants) {
				if (!participation.isHidden() && participation.getParticipant().isVisible()) {
					builder.append("<li><a href=\"http://device/participant/")
							.append(participation.getParticipant().getId()).append("\"><span");
					if (participation.isCancelled() || participation.getParticipant().isCancelled()) {
						builder.append(" class=\"cancelled\"");
					}
					builder.append(">");
					if (participation.isModerator()) {
						builder.append("M: ");
					}
					builder.append(participation.getParticipant().getName()).append("</span></a></li>");
				}
			}
			builder.append("</ul>");
		}

		builder.append("</div></body></html>");
		return builder.toString();
	}

	public String getLocationAndTrack() {
		StringBuilder buffer = new StringBuilder();
		buffer.append(getLocation().getName());
		String track = getTrackCategory();
		if (StringUtils.isNotEmpty(track)) {
			buffer.append(" | ").append(track);
		}
		return buffer.toString();
	}
	public String getSubTitle() {
		if (this.timeRange != null) {
			return getLocation().getName() + " | " + this.timeRange.asString();
		} else {
			return getLocation().getName();
		}
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public void setLocation(Location location) {
		this.location = location;

	}
	public Location getLocation() {
		return location;
	}
	public String getHashTag() {
		return hashTag;
	}
	public void setHashTag(String hashTag) {
		this.hashTag = hashTag;
	}
	public String getTrack() {
		return track;
	}
	public void setTrack(String track) {
		this.track = track;
	}
	public String getSortKey() {
		return sortKey;
	}
	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}
	public String getType() {
		if ("Teen Programming".equals(this.track)) {
			return this.track;
		} else {
			return type;
		}
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFullTitle() {
		if (StringUtils.isBlank(this.externalId)) {
			return this.title;
		} else {
			return "<b>" + this.externalId + "</b> " + this.title;
		}
	}
	public String getFullTitleNoHtml() {
		String fullTitle = getFullTitle();
		if (fullTitle.contains("<")) {
			boolean html = false;
			StringBuilder result = new StringBuilder();
			for (StringTokenizer tokenizer = new StringTokenizer(fullTitle, "<>", true); tokenizer.hasMoreTokens(); ) {
				String token = tokenizer.nextToken();
				if (">".equals(token) && html) {
					html = false;
				} else if (html) {
					// skip it
				} else if ("<".equals(token)) {
					html = true;
				} else {
					result.append(token);
				}
			}
			return result.toString();
		} else {
			return fullTitle;
		}
	}
	public void setTimeRange(TimeRange timeRange) {
		this.timeRange = timeRange;
	}
	public TimeRange getTimeRange() {
		return timeRange;
	}
	public List<Participation> getParticipants() {
		return participants;
	}
	public String toString() {
		return getFullTitleNoHtml();
	}
	@Override
	public int compareTo(Event another) {
		if (this.timeRange.compareTo(another.timeRange) == 0) {
			if (this.location.getSortKey() != 0 && another.location.getSortKey() != 0) {
				return this.location.getSortKey() - another.location.getSortKey();
			} else {
				return getSortKeyAsInt() - another.getSortKeyAsInt();
			}
		} else {
			return this.timeRange.compareTo(another.timeRange);
		}
	}

	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if (o == this) {
			return true;
		} else if (o.getClass() != getClass()) {
			return false;
		} else {
			Event that = (Event) o;
			return this.id.equals(that.id);
		}
	}

	public int hashCode() {
		return this.id == null ? 0 : this.id.hashCode();
	}
	public int getSortKeyAsInt() {
		try {
			return this.sortKey == null ? Integer.parseInt(this.externalId) : Integer.parseInt(this.sortKey);
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	public void setHighlighted(boolean highlight) {
		this.highlighted = highlight;
	}
	public boolean isHighlighted() {
		return this.highlighted;
	}
	public boolean isInSchedule() {
		return this.isInSchedule;
	}
	public void setInSchedule(boolean isInSchedule) {
		this.isInSchedule = isInSchedule;
	}
	public boolean isVisible() {
		return !isPrivate();
	}
	public boolean isPrivate() {
		return isPrivate;
	}
	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	public void setAlternate(boolean alternate) {
		this.alternate = alternate;
	}
	public boolean isAlternate() {
		return alternate;
	}
	public Participation getParticipation(Participant participant) {
		Participation result = null;
		for (Participation participation : this.participants) {
			if (participant.getId().equals(participation.getParticipant().getId())) {
				result = participation;
				break;
			}
		}
		return result;
	}
	public boolean isYay() {
		return yay;
	}
	public void setYay(boolean yay) {
		this.yay = yay;
	}
	public boolean isOmg() {
		return omg;
	}
	public void setOmg(boolean omg) {
		this.omg = omg;
	}
	public Ranking getRanking() {
		return ranking;
	}
	public void setRanking(Ranking ranking) {
		this.ranking = ranking;
	}
	public void setTouched(boolean touched) {
		this.touched = touched;
	}
	public boolean isTouched() {
		return touched;
	}

	public boolean matchesString(String searchTerm) {
		searchTerm = searchTerm.toLowerCase();
		if (this.title != null && this.title.toLowerCase().contains(searchTerm)) {
			return true;
		} else if (this.description != null && this.description.toLowerCase().contains(searchTerm)) {
			return true;
		} else {
			return matchesStringInParticipantName(searchTerm);
		}
	}

	boolean matchesStringInParticipantName(String searchTerm) {
		boolean result = false;
		for (Participation p : getParticipants()) {
			if (p.getParticipant().getName().toLowerCase().contains(searchTerm)) {
				result = true;
				break;
			}
		}
		return result;
	}

	public List<Event> getSubEvents() {
		return subEvents;
	}

	public void setSubEvents(List<Event> subEvents) {
		this.subEvents = subEvents;
	}

	public boolean isCancelled() {
		return this.cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public Set<Link> getLinks() {
		return links;
	}
}
