package org.ayizan.android.event.model;

import androidx.annotation.DrawableRes;

import org.ayizan.android.event.util.RenderContext;
import org.ayizan.android.event.util.StringUtils;
import org.ayizan.android.fogguide.R;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Participant implements Comparable<Participant>, Renderable, Groupable, Links {

	private String sortName;
	private String name;
	private String bio;
	private String id;
	private String pronouns;

	private List<Event> events = new ArrayList<Event>();
	private Set<Link> links = new LinkedHashSet<>();
	private boolean cancelled;

	public Participant(String id, String name, String sortName,
					   String bio, String pronouns) {
		this.id = id;
		this.name = name;
		this.sortName = sortName;
		this.bio = bio;
		this.pronouns = pronouns;
	}
	public Participant() {
	}
	public String getSortName() {
		if (this.sortName != null && Character.isLetter(this.sortName.charAt(0))) {
			return this.sortName;
		} else if (this.sortName != null && this.sortName.charAt(0) == '@') {
			return this.sortName.substring(1);
		} else {
			return this.name;
		}
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if ("ANONYMOUS".equals(name)) {
			this.name = "Anonymous";
		} else {
			this.name = name;
		}
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	public String getPronouns() {
		return pronouns;
	}
	public void setPronouns(String pronouns) {
		this.pronouns = pronouns;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String toString() {
		return getName();
	}
	public String getFullTitleNoHtml() {
		return this.name;
	}
	public String getGroup() {
		return getSortName().substring(0, 1).toUpperCase();
	}
	public int compareTo(Participant another) {
		if (this.sortName.toLowerCase().equals(another.sortName.toLowerCase())) {
			return this.id.compareTo(another.id);
		} else {
			return this.getSortName().toLowerCase().compareTo(another.getSortName().toLowerCase());
		}
	}

	public String getFullNameWithHtmlMarkup() {
		String firstName = getFirstName();
		if (StringUtils.isBlank(firstName)) {
			return "<b>" + this.name + "</b>";
		} else {
			return firstName + " <b>" + getLastName() + "</b>";
		}
	}

	public String getFullName() {
		String firstName = getFirstName();
		if (StringUtils.isBlank(firstName)) {
			return this.name;
		} else {
			return firstName + " " + getLastName();
		}
	}

	public String getLastName() {
		String firstName = getFirstName();
		if (StringUtils.isBlank(firstName)) {
			return this.name;
		} else {
			String lastName = this.name.substring(firstName.length());
			return lastName.trim();
		}
	}
	public String getFirstName() {
		String[] parts = this.sortName.split(" ");
		if (parts.length > 1) {
			String lastNameFirstPart = parts[0];
			if (lastNameFirstPart.endsWith(",")) {
				lastNameFirstPart = lastNameFirstPart.substring(0, lastNameFirstPart.length()-1);
			}
			int index = this.name.toUpperCase().lastIndexOf(lastNameFirstPart);
			if (lastNameFirstPart.startsWith("(") || lastNameFirstPart.startsWith("@")) {
				return null;
			} else if (index > 0) {
				return this.name.substring(0, index).trim();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public String getFullHtml(RenderContext renderContext) {
		StringBuilder builder = new StringBuilder();
		builder.append("<html><head><title>Bio</title><link rel=\"stylesheet\" type=\"text/css\" href=\"./list.css\" />")
				.append("</head><body>").append("<div class=\"content\"><div class=\"main\">")
				.append("<h2>").append(this.name).append("</h2>");
		if (renderContext.isImageAvailable()) {
			if (renderContext.isAddImageAllowed()) {
				builder.append("<a href=\"http://device/imagepicker/\"><img src=\"").append(renderContext.getImageSource()).append("\" class=\"bio-picture floatRight\" /></a>");
			} else {
				builder.append("<img src=\"").append(renderContext.getImageSource()).append("\" class=\"bio-picture floatRight\" />");
			}
		} else if (renderContext.isAddImageAllowed()) {
			builder.append("<a href=\"http://device/imagepicker/\"><img src=\"photo.png\" class=\"bio-picture floatRight\" style=\"width: 125px;\"/></a>");
		}
		
		if (this.bio != null) {
			builder.append(this.bio);
		}
		if (isCancelled()) {
			builder.append("<p class=\"notice\">").append(getName()).append(" is unable to attend.</p>");
		}
		
		builder.append("<div class=\"clear\"></div></div>");
		
		if (!this.events.isEmpty()) {
			builder.append("<ul class=\"nav\">");
			for (Event event : this.events) {
				if (event.isVisible() || event.isInSchedule()) {
					builder.append("<li><a href=\"http://device/event/").append(event.getId()).append("\"><span>")
						.append(event.getFullTitle()).append("</span></a></li>");
				}
			}
			builder.append("</ul>");
		}
		
		builder.append("</div></body></html>");
		
		return builder.toString();
	}
	public List<Event> getEvents() {
		return events;
	}
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
		
	}
	public boolean isCancelled() {
		return cancelled;
	}
	public boolean isVisible() {
		return !isPrivate();
	}
	public boolean isPrivate() {
		boolean result = true;
		for (Event event : this.events) {
			if (!event.isPrivate()) {
				Participation participation = event.getParticipation(this);
				if (!participation.isHidden()) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	public Set<Link> getLinks() {
		return links;
	}

	public @DrawableRes int getDefaultIcon() {
		return R.drawable.ic_participant;
	}

	public boolean isAnonymous() {
		return "Anonymous".equalsIgnoreCase(this.name);
	}
}
