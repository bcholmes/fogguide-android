package org.ayizan.android.fogguide.view;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.service.EventService;
import org.ayizan.android.fogguide.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LeaderboardFragment extends Fragment {

	private RecyclerView recyclerView;
	private RecyclerView.Adapter adapter;
	private RecyclerView.LayoutManager layoutManager;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.simple_recycler_layout, container, false);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		this.recyclerView = (RecyclerView) getView().findViewById(R.id.simple_list);
		this.recyclerView.setHasFixedSize(true);

		// use a linear layout manager
		this.layoutManager = new LinearLayoutManager(getActivity());
		this.recyclerView.setLayoutManager(this.layoutManager);

		DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
		this.adapter = new LeaderboardEventViewAdapter(metrics, this.recyclerView, getEvents(), new EventOpener(getActivity()));
		this.recyclerView.setAdapter(this.adapter);
	}

	protected List<Event> getEvents() {
		List<Event> result = new ArrayList<>();
		for (Event event : getEventService().getLeafProgramItems()) {
			if (event.getRanking().getScore() > 0) {
				result.add(event);
			}
		}
		Collections.sort(result, new Comparator<Event>() {

			@Override
			public int compare(Event lhs, Event rhs) {
				if (lhs.getRanking().getScore() == rhs.getRanking().getScore()) {
					return lhs.getSortKeyAsInt() - rhs.getSortKeyAsInt();
				} else {
					return rhs.getRanking().getScore() - lhs.getRanking().getScore();
				}
			}
			
		});
		return result;
	}

	private EventService getEventService() {
		BaseEventApplication application = (BaseEventApplication) getActivity().getApplication();
		return application.getEventService();
	}
}
