package org.ayizan.android.fogguide.view;

import static org.ayizan.android.event.BaseEventApplication.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.model.TimeRange;
import org.ayizan.android.event.service.EventService;
import org.ayizan.android.fogguide.R;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProgramFragment extends EventListFragment implements SearchView.OnQueryTextListener {

	private static final long FIVE_MINUTES = 5L * 60L * 1000L;
	private Date lastAutoScroll;

	SearchView searchView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_search, menu);

		MenuItem searchItem = menu.findItem(R.id.search);
		this.searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
		this.searchView.setOnQueryTextListener(this);
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(
//                new ComponentName(this, SearchableActivity.class)));
//        searchView.setIconifiedByDefault(false);

	}
	public void onStart() {
		super.onStart();

		if (this.lastAutoScroll == null || (this.lastAutoScroll.getTime() < (System.currentTimeMillis() - FIVE_MINUTES))) {
			this.lastAutoScroll = new Date();

			scrollToNow();
		}
	}

	void scrollToNow() {
		EventService eventService = ((BaseEventApplication) getActivity().getApplication()).getEventService();
		TimeRange timeRange = eventService.getClosestTimeRangeToNow();
		if (timeRange != null) {
			List<Event> events = this.adapter.getEvents();
			int position = 0;
			for (Event event : events) {
				if (timeRange.equals(event.getTimeRange())) {
					break;
				} else {
					position++;
				}
			}

			// BCH: so long as we're scrolling to the item with the section header,
			//      we don't need to account for the height of the header.
			this.layoutManager.scrollToPositionWithOffset(position, 0);
		}
	}

	List<Event> filteredEvents(String searchTerm) {
		List<Event> result = new ArrayList<>();
		for (Event e : getEvents()) {
			if (e.matchesString(searchTerm)) {
				result.add(e);
			}
		}
		return result;
	}


	@Override
	public boolean onQueryTextSubmit(String query) {
		// User pressed the search button
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		Log.i(TAG, newText);
		if (newText != null && newText.length() > 0) {
			this.adapter.replaceAll(filteredEvents(newText));
			this.headerDecoration.clearHeaderCache();
		} else {
			this.adapter.replaceAll(getEvents());
			this.headerDecoration.clearHeaderCache();
		}
		return false;
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		// BCH: if we're in the midst of a search, don't touch the list due to a data refresh
		if (this.searchView.isIconified()) {
			super.propertyChange(event);
		}
	}
}
