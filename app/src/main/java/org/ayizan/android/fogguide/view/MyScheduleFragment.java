package org.ayizan.android.fogguide.view;

import org.ayizan.android.event.model.Event;

import java.util.ArrayList;
import java.util.List;

public class MyScheduleFragment extends EventListFragment {

	@Override
	protected List<Event> getEvents() {
		List<Event> events = super.getEvents();
		ArrayList<Event> result = new ArrayList<Event>();
		for (Event event : events) {
			if (event.isInSchedule()) {
				result.add(event);
			}
		}
		return result;
	}
}
