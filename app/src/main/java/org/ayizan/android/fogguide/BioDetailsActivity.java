package org.ayizan.android.fogguide;

import android.util.Log;

import org.ayizan.android.event.model.Participant;


public class BioDetailsActivity extends DetailsActivity {
	
	@Override
	public boolean isImageAvailable() {
		FogGuideApplication application = (FogGuideApplication) getApplication();
		return application.getImageService().isAvatarAvailable(getParticipant());
	}
 
	@Override
	public String getImageSource() {
		FogGuideApplication application = (FogGuideApplication) getApplication();
		return application.getImageService().getAvatarAsEncodedHtmlImage(getParticipant());
	}

	private Participant getParticipant() {
		return (Participant) getRenderable();
	}
	
	public boolean isAddImageAllowed() {
		FogGuideApplication application = (FogGuideApplication) getApplication();
		Log.i("FogGuide", "Person id is " + application.getPersonId());
		if (application.isLoggedIn() && getParticipant().getId() != null && getParticipant().getId().equals(application.getPersonId())) {
			return true;
		} else {
			return false;
		}
	}
}
