package org.ayizan.android.fogguide.view.bio;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import com.google.android.material.appbar.AppBarLayout;
import android.widget.Space;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.model.Link;
import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.service.ImageService;
import org.ayizan.android.event.util.PersonIdProvider;
import org.ayizan.android.fogguide.DetailsActivity;
import org.ayizan.android.fogguide.R;
import org.ayizan.android.fogguide.util.AppBarStateChangeListener;
import org.ayizan.android.fogguide.util.Utils;
import org.ayizan.android.fogguide.view.EventOpener;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by bcholmes on 2018-03-10.
 */
public class ParticipantBioActivity extends AppCompatActivity {

    private final static float EXPAND_AVATAR_SIZE_DP = 80f;
    private final static float COLLAPSED_AVATAR_SIZE_DP = 32f;

    private AppBarLayout mAppBarLayout;
    private CircleImageView mAvatarImageView;
    private TextView mToolbarTextView, mTitleTextView;
    private Space mSpace;
    private Toolbar mToolBar;

    private RecyclerView mRecyclerView;
    private AppBarStateChangeListener mAppBarStateChangeListener;

    private int[] mAvatarPoint = new int[2], mSpacePoint = new int[2], mToolbarTextPoint =
            new int[2], mTitleTextViewPoint = new int[2];
    private float mTitleTextSize;

    private PersonIdProvider personIdProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_details);

        this.personIdProvider = (PersonIdProvider) getApplication();

        findViews();
        setUpViews();

        populateContents();
    }

    protected void launchImagePicker() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, DetailsActivity.RESULT_LOAD_IMAGE);
    }

    private void populateContents() {
        Participant participant = getParticipant();

        mTitleTextView.setText(Html.fromHtml(participant.getFullName()));
        populateAvatar();

        TextView bio = findViewById(R.id.bio);
        if (participant.getBio() != null && participant.getBio().trim().length() > 0) {
            bio.setText(Html.fromHtml(participant.getBio()));
            bio.setVisibility(View.VISIBLE);
        } else {
            bio.setVisibility(View.GONE);
        }


        LinearLayout linksLayout = findViewById(R.id.link_list);
        if (participant.getLinks().isEmpty()) {
            linksLayout.setVisibility(View.GONE);
        } else {
            for (final Link link: participant.getLinks()) {
                View view = getLayoutInflater().inflate(R.layout.simple_item_layout, null);
                ImageView image = view.findViewById(R.id.icon);
                image.setImageResource(link.getType().getIcon());

                TextView textView = view.findViewById(R.id.label);
                textView.setText(Html.fromHtml(link.getName()));
                textView.setSingleLine();
                textView.setEllipsize(TextUtils.TruncateAt.END);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getHref()));
                            startActivity(myIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(ParticipantBioActivity.this, "Oh dear. Horrible horrible things have happened",
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                });
                linksLayout.addView(view);
            }
        }

        TextView cancelledMessage = findViewById(R.id.cancelled_message);
        if (participant.isCancelled()) {
            cancelledMessage.setText(Html.fromHtml(
                    "<i>" + getString(R.string.cancelled_message, participant.getFullName() + "</i>")));
            cancelledMessage.setVisibility(View.VISIBLE);
        }

        LinearLayout layout = findViewById(R.id.event_list);

        ProgramItemPopulator populator = new ProgramItemPopulator(this.personIdProvider, getParticipant());
        for (final Event event: participant.getEvents()) {
            View view = getLayoutInflater().inflate(R.layout.program_item_layout, null);
            populator.populateProgramListItem(view, event, getResources());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //view.playSoundEffect(SoundEffectConstants.CLICK);
                    view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                    new EventOpener(ParticipantBioActivity.this).open(event);
                }
            });
            layout.addView(view);
        }
    }

    private void populateAvatar() {
        Participant participant = getParticipant();
        ImageService imageService = ((BaseEventApplication) getApplication()).getImageService();
        if (imageService.isAvatarAvailable(participant)) {
            mAvatarImageView.setImageURI(Uri.fromFile(imageService.fileForParticipant(participant)));
        } else {
            mAvatarImageView.setImageResource(participant.getDefaultIcon());
        }
    }

    public Participant getParticipant() {
        BaseEventApplication application = (BaseEventApplication) getApplication();
        String entityId = getIntent().getStringExtra("ENTITY_ID");
        return application.getEventService().getParticipantById(entityId);
    }

    private void findViews() {
        mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mAvatarImageView = (CircleImageView) findViewById(R.id.imageView_avatar);
        mToolbarTextView = (TextView) findViewById(R.id.toolbar_title);
        mTitleTextView = (TextView) findViewById(R.id.textView_title);
        mSpace = (Space) findViewById(R.id.space);
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
    }

    private void setUpViews() {
        mTitleTextSize = mTitleTextView.getTextSize();
        setUpToolbar();
        setUpAmazingAvatar();
        setUpImagePickerButton();
    }

    private void setUpImagePickerButton() {
        View button = findViewById(R.id.btn_choose_avatar);
        if (isAddImageAllowed()) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchImagePicker();
                }
            });
        } else {
            button.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == DetailsActivity.RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {

            String picturePath = getPicturePath(data);

            if (picturePath != null) {
                Bitmap bitmap = BitmapFactory.decodeFile(picturePath);

                if (bitmap != null && bitmap.getWidth() > 200) {
                    Bitmap resized = Bitmap.createScaledBitmap(bitmap, 200,
                            (int) Math.floor(bitmap.getHeight() * (200.0 / bitmap.getHeight())), true);
                    bitmap = resized;
                }

                if (bitmap != null) {
                    Log.d(BaseEventApplication.TAG, "Image picked: " + bitmap.getHeight() + "x" + bitmap.getWidth());
                    BaseEventApplication application = (BaseEventApplication) getApplication();
                    try {
                        application.getImageService().writeImageToFileSystem(bitmap, getParticipant());
                        populateAvatar();
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(),
                                "We encountered a problem trying to save your avatar.", Toast.LENGTH_LONG).show();
                    }
                }
                // String picturePath contains the path of selected Image
            } else {
                Toast.makeText(getApplicationContext(), "Sorry. That picture isn't our friend.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setUpToolbar() {
        setSupportActionBar(mToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setUpAmazingAvatar() {
        mAppBarStateChangeListener = new AppBarStateChangeListener() {

            @Override
            public void onStateChanged(AppBarLayout appBarLayout,
                                       AppBarStateChangeListener.State state) {
            }

            @Override
            public void onOffsetChanged(AppBarStateChangeListener.State state, float offset) {
                translationView(offset);
            }
        };
        mAppBarLayout.addOnOffsetChangedListener(mAppBarStateChangeListener);
    }


    private void translationView(float offset) {
        float xOffset = -(mAvatarPoint[0] - mSpacePoint[0]) * offset;
        float yOffset = -(mAvatarPoint[1] - mSpacePoint[1]) * offset;
        float xTitleOffset = -(mTitleTextViewPoint[0] - mToolbarTextPoint[0]) * offset;
        float yTitleOffset = -(mTitleTextViewPoint[1] - mToolbarTextPoint[1]) * offset;
        int newSize = Utils.convertDpToPixelSize(
                EXPAND_AVATAR_SIZE_DP - (EXPAND_AVATAR_SIZE_DP - COLLAPSED_AVATAR_SIZE_DP) * offset,
                ParticipantBioActivity.this);
        float newTextSize =
                mTitleTextSize - (mTitleTextSize - mToolbarTextView.getTextSize()) * offset;
        mAvatarImageView.getLayoutParams().width = newSize;
        mAvatarImageView.getLayoutParams().height = newSize;
        mAvatarImageView.setTranslationX(xOffset);
        mAvatarImageView.setTranslationY(yOffset);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, newTextSize);
        mTitleTextView.setTranslationX(xTitleOffset);
        mTitleTextView.setTranslationY(yTitleOffset);
    }

    private void clearAnim() {
        mAvatarImageView.setTranslationX(0);
        mAvatarImageView.setTranslationY(0);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTitleTextSize);
        mTitleTextView.setTranslationX(0);
        mTitleTextView.setTranslationY(0);
    }

    private void resetPoints() {
        clearAnim();

        int avatarSize = Utils.convertDpToPixelSize(EXPAND_AVATAR_SIZE_DP, this);
        mAvatarImageView.getLocationOnScreen(mAvatarPoint);
        mAvatarPoint[0] -= (avatarSize - mAvatarImageView.getWidth()) / 2;
        mSpace.getLocationOnScreen(mSpacePoint);
        mToolbarTextView.getLocationOnScreen(mToolbarTextPoint);
        mToolbarTextPoint[0] += Utils.convertDpToPixelSize(16, this);
        mTitleTextView.post(new Runnable() {

            @Override
            public void run() {
                mTitleTextView.getLocationOnScreen(mTitleTextViewPoint);
                translationView(mAppBarStateChangeListener.getCurrentOffset());
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            resetPoints();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private String getPicturePath(Intent data) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }

    public boolean isAddImageAllowed() {
        return false;
    }
}
