package org.ayizan.android.fogguide;

import java.io.IOException;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.util.RenderContext;

public abstract class DetailsActivity extends AppCompatActivity implements HashTagProvider, RenderContext {
	
	public static final int RESULT_LOAD_IMAGE = 8675309;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_details);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		WebView webView = (WebView) findViewById(R.id.bio_details);
        webView.setBackgroundColor(0x00000000); 
        
        webView.setWebViewClient(new FogGuideWebViewClient(this, this));
        refreshWebView();
	}

	public String getHashTag() {
		return "";
	}

	public Renderable getRenderable() {
		FogGuideApplication application = (FogGuideApplication) getApplication();
		String entityId = getIntent().getStringExtra("ENTITY_ID");
        EntityType entityType = (EntityType) getIntent().getSerializableExtra("ENTITY_TYPE");
        if (entityType == EntityType.RESTAURANT) {
	        Renderable renderable = application.getRestaurantService().getRestaurantById(entityId);
			return renderable;
        } else if (entityType == EntityType.ANNOUNCEMENT) {
			Renderable renderable = application.getAnnouncementService().getAnnouncementById(entityId);
			return renderable;
        } else {
	        Renderable renderable = application.getEventService().getEntityById(entityType, entityId);
			return renderable;
		}
	}
	
	protected void refreshWebView() {
        WebView webView = (WebView) findViewById(R.id.bio_details);
		String participantId = getIntent().getStringExtra("ENTITY_ID");
        EntityType entityType = (EntityType) getIntent().getSerializableExtra("ENTITY_TYPE");
        Renderable renderable = getRenderable();
        
        webView.loadDataWithBaseURL("file:///android_asset/details_" + entityType.name().toLowerCase() 
        		+ "_" + participantId, renderable.getFullHtml(this), "text/html", "UTF-8", "");
	}
	
	public boolean isImageAvailable() {
		return false;
	}
	public String getImageSource() {
		return null;
	}
	
	public boolean isAddImageAllowed() {
		return false;
	}

	public String getPersonId() {
		FogGuideApplication application = (FogGuideApplication) getApplication();
		return application.getPersonId();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
				&& null != data) {

			String picturePath = getPicturePath(data);
			
			if (picturePath != null) {
				Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
				
				if (bitmap != null && bitmap.getWidth() > 200) {
					Bitmap resized = Bitmap.createScaledBitmap(bitmap, 200, 
							(int) Math.floor(bitmap.getHeight() * (200.0 / bitmap.getHeight())), true);
					bitmap = resized;
				}
				
				if (bitmap != null) {
					Log.d("WisSched", "Image picked: " + bitmap.getHeight() + "x" + bitmap.getWidth());
					
					FogGuideApplication application = (FogGuideApplication) getApplication();
					try {
						application.getImageService().writeImageToFileSystem(bitmap, (Participant) getRenderable());
						
						refreshWebView();
					} catch (IOException e) {
						Toast.makeText(getApplicationContext(), 
								"We encountered a problem trying to save your avatar.", Toast.LENGTH_LONG).show();
					}
				}
				// String picturePath contains the path of selected Image
			} else {
				Toast.makeText(getApplicationContext(), "Sorry. That picture isn't our friend.", Toast.LENGTH_LONG).show();
			}
		}
	}

	private String getPicturePath(Intent data) {
		Uri selectedImage = data.getData();
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		Cursor cursor = getContentResolver().query(selectedImage,
				filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String picturePath = cursor.getString(columnIndex);
		cursor.close();
		return picturePath;
	}
}
