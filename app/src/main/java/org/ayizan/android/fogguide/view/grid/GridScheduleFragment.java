package org.ayizan.android.fogguide.view.grid;

import android.graphics.RectF;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.model.ScheduledItem;
import org.ayizan.android.event.service.GridViewService;
import org.ayizan.android.event.util.DateFlyweight;
import org.ayizan.android.fogguide.R;
import org.ayizan.android.fogguide.view.EventOpener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by BC Holmes on 2018-02-19.
 */
public class GridScheduleFragment extends Fragment {

    private String selectedDay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.getString("selectedDay") != null) {
            this.selectedDay = savedInstanceState.getString("selectedDay");
        }
        View view = inflater.inflate(R.layout.grid_schedule_layout, container, false);

        populateDayChooser(inflater, container, view, getGridViewService().getDays());
        prepareGridView(inflater, view, this.selectedDay);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("selectedDay", this.selectedDay);
        super.onSaveInstanceState(outState);
    }

    private void prepareGridView(LayoutInflater inflater, View view, String day) {
        ViewGroup group = view.findViewById(R.id.grid_view_container);
        group.removeAllViews();

        GridView gridView = (GridView) inflater.inflate(R.layout.grid_view, group, false);

        gridView.setOnEventClickListener(new GridView.EventClickListener() {
            @Override
            public void onEventClick(GridViewEvent event, RectF eventRect) {

            }
        });

        List<ScheduledItem> eventsForDay = getGridViewService().getItemsForDay(day);
//                filterByDay(eventService.getTopLevelProgramItems(), day);

        gridView.setOnEventClickListener(new GridView.EventClickListener() {
            @Override
            public void onEventClick(GridViewEvent event, RectF eventRect) {
                launchEventActivity(event.item);
            }
        });

        gridView.setGridDataProvider(new GridDataProvider(getGridViewService().getAllLocations(), eventsForDay));
        gridView.setLimitTime(getStartHour(eventsForDay), getEndHour(eventsForDay));

        group.addView(gridView);
    }

    private int getStartHour(List<ScheduledItem> eventsForDay) {
        if (eventsForDay.size() == 0) {
            return 0;
        } else {
            int result = 999;
            for (ScheduledItem item: eventsForDay) {
                int startHour = getHourFor(item.getTimeRange().getStartTime());
                if (item.getTimeRange().isMidnightStart()) {
                    startHour += 24;
                }
                result = Math.min(result, startHour);
            }
            return result;
        }
    }

    private int getEndHour(List<ScheduledItem> eventsForDay) {
        if (eventsForDay.size() == 0) {
            return 24;
        } else {
            int result = 0;
            for (ScheduledItem event: eventsForDay) {
                int endHour = getHourFor(event.getTimeRange().getEndTime());
                if (event.getTimeRange().isMidnightStart() || event.getTimeRange().isSpanningMidnight()) {
                    endHour += 24;
                }
                if (getMinFor(event.getTimeRange().getEndTime()) != 0) {
                    endHour += 1;
                }
                result = Math.max(result, endHour);
            }
            return result;
        }
    }

    private int getHourFor(Date date) {
        try {
            return Integer.parseInt(DateFlyweight.getFormatter("H").format(date));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private int getMinFor(Date date) {
        try {
            return Integer.parseInt(DateFlyweight.getFormatter("m").format(date));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private void launchEventActivity(ScheduledItem event) {
        if (event instanceof Event) {
            new EventOpener(getActivity()).open((Event) event);
/*
        } else if (event instanceof SimpleScheduledItem) {
            ReferenceItem item = getReferenceService().getReferenceItemByKey(((SimpleScheduledItem) event).getHref());
            if (item != null) {
                new ReferenceItemOpener(getActivity()).open(item);
            } else {
                Log.w("WisSched","item " + ((SimpleScheduledItem) event).getId() + " not found");
            }
 */
        }
    }

//    private ReferenceService getReferenceService() {
//        return ((FogGuideApplication) getActivity().getApplication()).getReferenceService();
//    }

    private void populateDayChooser(final LayoutInflater inflater, ViewGroup container, View view, List<String> days) {
        String today = DateFlyweight.getFormatter("EEEE").format(new Date());
        if (this.selectedDay != null) {
            today = selectedDay;
        } else if (!days.contains(today)) {
            today = days.get(0);
        }
        final List<Button> buttons = new ArrayList<>();
        LinearLayout dayChooser = view.findViewById(R.id.day_selector);
        for (final String day: days) {
            final Button button = (Button) inflater.inflate(R.layout.day_chooser_button, container, false);
            button.setText(day.substring(0, 2));
            button.setSelected(day.equals(today));
            button.setTag(day);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (Button b: buttons) {
                        if (b == button) {
                            b.setSelected(true);
                        } else {
                            b.setSelected(false);
                        }
                    }
                    selectDay(inflater, getView(), day);
                }
            });
            ViewGroup.LayoutParams params = button.getLayoutParams();
            LinearLayout.LayoutParams newParams = new LinearLayout.LayoutParams(params);
            newParams.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
            newParams.rightMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());
            button.setLayoutParams(newParams);
            buttons.add(button);

            dayChooser.addView(button);
        }
        this.selectedDay = today;
    }

    void selectDay(final LayoutInflater inflater, final View view, String day) {
        this.selectedDay = day;
        prepareGridView(inflater, view, day);
    }

    public GridViewService getGridViewService() {
        BaseEventApplication application = (BaseEventApplication) getActivity().getApplication();
        return application.getGridViewService();
    }
}