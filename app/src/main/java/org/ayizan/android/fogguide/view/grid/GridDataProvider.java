package org.ayizan.android.fogguide.view.grid;

import org.ayizan.android.event.model.Location;
import org.ayizan.android.event.model.ScheduledItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by bcholmes on 2018-02-23.
 */
public class GridDataProvider implements ColumnHeaderProvider {

    private List<Location> locations = new ArrayList<>();
    private List<ScheduledItem> events = new ArrayList<>();

    public GridDataProvider(List<Location> locations, List<ScheduledItem> events) {
        this.events = events;
        this.locations = extractLocations(events);
    }

    private List<Location> extractLocations(List<ScheduledItem> events) {
        Set<Location> set = new HashSet<>();
        for (ScheduledItem event: events) {
            set.addAll(event.getLocation().getAllLocations());
        }
        List<Location> result = new ArrayList<>(set);
        Collections.sort(result);
        return result;
    }

    public String getHeaderLabel(int columnNumber) {
        if (columnNumber >= 0 && columnNumber < this.locations.size()) {
            return this.locations.get(columnNumber).getDisplayName();
        } else {
            return null;
        }
    }

    public int getNumberOfColumns() {
        return this.locations.size();
    }

    public Location getLocation(int columnNumber) {
        if (columnNumber >= 0 && columnNumber < this.locations.size()) {
            return this.locations.get(columnNumber);
        } else {
            return null;
        }
    }

    public List<GridViewEvent> getAllEvents() {
        List<GridViewEvent> result = new ArrayList<>();
        for (ScheduledItem event : this.events) {
            result.add(new GridViewEvent(event, this.locations.indexOf(event.getLocation().getAllLocations().get(0))+1));
        }

        return result;
    }
}
