package org.ayizan.android.fogguide.view;

import android.graphics.Color;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.util.view.ItemOpener;
import org.ayizan.android.fogguide.R;

import java.util.List;

/**
 * Created by BC Holmes on 2016-08-18.
 */
class LeaderboardEventViewAdapter extends BaseRecordViewAdapter<Event> {

    private int width;
    private float highestScore;

    public LeaderboardEventViewAdapter(DisplayMetrics displayMetrics, RecyclerView recyclerView,
                                       List<Event> events,
                                       ItemOpener<Event> opener) {
        super(recyclerView, events, opener);

        this.width = Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.highestScore = roundUp(events.isEmpty() ? 10 : events.get(0).getRanking().getScore());

    }

    private int roundUp(int n) {
        if (n < 10) {
            return n+1;
        } else if (n < 25) {
            return (int) Math.ceil((n + 1.0) / 5.0 * 5.0);
        } else {
            return (int) Math.ceil((n + 1.0) / 10.0 * 10.0);
        }
    }

    @Override
    public BaseRecordViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leaderboard_item_layout, parent, false);
        return new ViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Event event = this.records.get(position);

        View view = holder.view;
        TextView textView = (TextView) view.findViewById(R.id.label);
        textView.setText(Html.fromHtml(event.getFullTitle()));
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);

        TextView subLabel = (TextView) view.findViewById(R.id.sub_label);
        assignBackgroundColor(view, event);

        int id = EventListAdapter.getEventIconId(event);
        ImageView icon = (ImageView) view.findViewById(R.id.panel_type_icon);
        icon.setImageResource(id);
        subLabel.setText(event.getLocationAndTrack());
        subLabel.setSingleLine();
        subLabel.setEllipsize(TextUtils.TruncateAt.END);


        View barView = view.findViewById(R.id.scoreindicator);

        ViewGroup.LayoutParams layoutParams = barView.getLayoutParams();
        layoutParams.width = Math.round(this.width * ((float) event.getRanking().getScore()) / this.highestScore);
        barView.setLayoutParams(layoutParams);

    }

    protected void assignBackgroundColor(View view, Event event) {
        if (event.isHighlighted()) {
            view.setBackgroundColor(Color.YELLOW);
        } else {
            view.setBackgroundColor(Color.WHITE);
        }
    }

}
