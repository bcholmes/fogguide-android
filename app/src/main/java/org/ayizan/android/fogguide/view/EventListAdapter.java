package org.ayizan.android.fogguide.view;

import android.graphics.Color;
import androidx.recyclerview.widget.SortedList;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Paint;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.util.PersonIdProvider;
import org.ayizan.android.event.util.view.ItemOpener;
import org.ayizan.android.fogguide.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BC Holmes on 2016-08-19.
 */
public class EventListAdapter extends BaseStickyHeaderRecordAdapter<Event> {

    PersonIdProvider personIdProvider;
    SortedList<Event> sortedList = new SortedList<Event>(Event.class, new SortedList.Callback<Event>() {
        @Override
        public int compare(Event a, Event b) {
            return a.compareTo(b);
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(Event oldItem, Event newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(Event item1, Event item2) {
            return item1 == item2;
        }
    });

    EventListAdapter(RecyclerView recyclerView,
                     List<Event> records,
                     ItemOpener<Event> opener, PersonIdProvider personIdProvider) {
        super(recyclerView, null, opener);
        this.personIdProvider = personIdProvider;
        replaceAll(records);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.program_item_layout, parent, false);
        return new ViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        View view = holder.view;

        Event event = getRecord(position);

        TextView textView = (TextView) view.findViewById(R.id.label);
        textView.setText(Html.fromHtml(event.getFullTitle()));
        if (event.isCancelled()) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(0xffbbbbbb);
        } else {
            textView.setPaintFlags(textView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(0x8a000000);
        }
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);

        TextView subLabel = (TextView) view.findViewById(R.id.sub_label);
        assignBackgroundColor(view, event);

        int id = getEventIconId(event);
        ImageView icon = (ImageView) view.findViewById(R.id.panel_type_icon);
        icon.setImageResource(id);
        subLabel.setText(event.getLocationAndTrack());
        subLabel.setSingleLine();
        subLabel.setEllipsize(TextUtils.TruncateAt.END);

    }

    @Override
    public int getItemCount() {
        return this.sortedList.size();
    }

    public Event getRecord(int position) {
        return this.sortedList.get(position);
    }

    public static int getEventIconId(Event event) {
        int id = R.drawable.ic_event_special;
        if (event.isInSchedule()) {
            id = R.mipmap.ic_calendar;
        } else if ("workshop".equalsIgnoreCase(event.getType())) {
            id = R.drawable.ic_event_workshop;
        } else if (event.getFullTitle().contains("Dinner")) {
            id = R.drawable.ic_event_meal;
        } else if ("party".equalsIgnoreCase(event.getType())) {
            id = R.mipmap.icon_party;
        } else if ("special".equalsIgnoreCase(event.getType())) {
            id = R.drawable.ic_event_special;
        } else if ("reading".equalsIgnoreCase(event.getType())) {
            id = R.drawable.ic_event_reading;
        } else if ("academic".equalsIgnoreCase(event.getType())) {
            id = R.mipmap.icon_academic;
        } else if ("meal".equalsIgnoreCase(event.getType())) {
            id = R.drawable.ic_event_meal;
        } else if ("gaming".equalsIgnoreCase(event.getType())) {
            id = R.drawable.ic_event_gaming;
        } else if ("panel".equalsIgnoreCase(event.getType()) || "program".equalsIgnoreCase(event.getType())) {
            id = R.drawable.ic_event_panel;
        }
        return id;
    }

    protected void assignBackgroundColor(View view, Event event) {
        if (event.isHighlighted()) {
            view.setBackgroundColor(Color.YELLOW);
        } else if (this.personIdProvider != null  && event.isMyEvent(this.personIdProvider)) {
            view.setBackgroundColor(Color.rgb(209, 212, 239));
        } else {
            view.setBackgroundColor(Color.WHITE);
        }
    }

    public List<Event> getEvents() {
        List<Event> result = new ArrayList<>();
        for (int i = 0; i < this.sortedList.size(); i++) {
            result.add(this.sortedList.get(i));
        }
        return result;
    }

    public void replaceAll(List<Event> events) {
        this.sortedList.beginBatchedUpdates();
        for (int i = this.sortedList.size()-1; i >=0; i--) {
            Event event = this.sortedList.get(i);
            if (!events.contains(event)) {
                this.sortedList.remove(event);
            }
        }
        this.sortedList.addAll(events);
        this.sortedList.endBatchedUpdates();
        initializeHeaders(events);
    }

}
