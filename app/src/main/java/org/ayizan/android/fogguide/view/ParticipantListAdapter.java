package org.ayizan.android.fogguide.view;

import android.content.Context;
import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.service.ImageService;
import org.ayizan.android.event.util.view.ItemOpener;
import org.ayizan.android.fogguide.R;

import java.util.List;

/**
 * Created by BC Holmes on 2016-08-19.
 */
class ParticipantListAdapter extends BaseStickyHeaderRecordAdapter<Participant> {

    private LayoutInflater inflater;
    private ImageService imageService;

    public ParticipantListAdapter(Context context, RecyclerView recyclerView, ImageService imageService, List<Participant> participants, ItemOpener<Participant> opener) {
        super(recyclerView, participants, opener);
        this.inflater = LayoutInflater.from(context);
        this.imageService = imageService;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final View view = this.inflater.inflate(R.layout.participant_list_item, viewGroup, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        Participant participant = this.records.get(position);

        TextView textView = (TextView) viewHolder.view.findViewById(R.id.label);
        textView.setText(Html.fromHtml(participant.getFullNameWithHtmlMarkup()));
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);

        ImageView imageView = (ImageView) viewHolder.view.findViewById(R.id.avatar_icon);
        if (this.imageService.isAvatarAvailable(participant)) {
            imageView.setImageURI(Uri.fromFile(this.imageService.fileForParticipant(participant)));
        } else {
            imageView.setImageResource(R.drawable.ic_participant);
        }
    }

    List<Participant> getParticipants() {
        return this.records;
    }

    void setParticipants(List<Participant> participants) {
        this.records = participants;
        initializeHeaders(participants);
    }
}
