package org.ayizan.android.fogguide.view;

import android.app.Activity;
import android.content.Intent;

import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.util.view.ItemOpener;
import org.ayizan.android.fogguide.view.event.EventDetailsActivity;

/**
 * Created by BC Holmes on 2016-08-18.
 */
public class EventOpener implements ItemOpener<Event> {

    Activity activity;

    public EventOpener(Activity activity) {
        this.activity = activity;
    }
    @Override
    public void open(Event item) {
        Intent intent = new Intent(activity, EventDetailsActivity.class);
        intent.putExtra("ENTITY_ID", item.getId());
        intent.putExtra("ENTITY_TYPE", EntityType.EVENT);
        activity.startActivity(intent);

    }
}
