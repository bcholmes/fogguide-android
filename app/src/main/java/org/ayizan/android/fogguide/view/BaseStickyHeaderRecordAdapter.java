package org.ayizan.android.fogguide.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.ayizan.android.event.model.Groupable;
import org.ayizan.android.event.util.view.ItemOpener;
import org.ayizan.android.fogguide.R;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;

/**
 * Created by BC Holmes on 2016-08-19.
 */
abstract class BaseStickyHeaderRecordAdapter<T extends Groupable> extends BaseRecordViewAdapter<T> implements
        StickyHeaderAdapter<BaseStickyHeaderRecordAdapter.HeaderHolder> {

    LinkedHashSet<String> headerValues = new LinkedHashSet<>();

    BaseStickyHeaderRecordAdapter(RecyclerView recyclerView, List<T> records, ItemOpener<T> opener) {
        super(recyclerView, records, opener);
        initializeHeaders(records);
    }

    void initializeHeaders(Iterable<T> records) {
        this.headerValues.clear();
        if (records != null) {
            for (Groupable g : records) {
                this.headerValues.add(g.getGroup());
            }
        }
    }

    @Override
    public long getHeaderId(int position) {
        List<String> headers = new ArrayList<>(this.headerValues);
        Groupable groupable = getRecord(position);

        return (long) headers.indexOf(groupable.getGroup());
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup parent) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_header, parent, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder viewholder, int position) {
        Groupable groupable = getRecord(position);
        viewholder.header.setText(groupable.getGroup());
    }


    static class HeaderHolder extends RecyclerView.ViewHolder {
        TextView header;

        public HeaderHolder(View itemView) {
            super(itemView);

            header = (TextView) itemView;
        }
    }
}
