package org.ayizan.android.fogguide.view.event;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.model.Link;
import org.ayizan.android.event.model.Location;
import org.ayizan.android.event.model.Participation;
import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.service.EventService;
import org.ayizan.android.event.util.PersonIdProvider;
import org.ayizan.android.fogguide.FogGuideApplication;
import org.ayizan.android.fogguide.R;
import org.ayizan.android.fogguide.view.EventListAdapter;
import org.ayizan.android.fogguide.view.EventOpener;
import org.ayizan.android.fogguide.view.ParticipantItemOpener;
import org.ayizan.android.fogguide.view.bio.ProgramItemPopulator;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class EventDetailsActivity extends AppCompatActivity implements PropertyChangeListener {

	private Menu menu;
    private FirebaseAnalytics firebaseAnalytics;


    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.event_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

		getEventService().addPropertyChangeListener(this);
        setUpButtonListeners();

        populateContents();
        this.firebaseAnalytics = FirebaseAnalytics.getInstance(this);
	}

	private void setUpButtonListeners() {
        ((ToggleButton) findViewById(R.id.btn_highlighter)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                toggleHighlight(isChecked);
            }
        });
        ((ToggleButton) findViewById(R.id.btn_calendar)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                toggleScheduled(isChecked);
            }
        });
        ((ToggleButton) findViewById(R.id.btn_yay)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                toggleYay(isChecked);
            }
        });
    }

	private void setUpToolbarState() {
        setUpScheduledToolbarState();
        setUpHighlightToolbarState();
        setUpYayToolbarState();
	}

    private void setUpScheduledToolbarState() {
        boolean isInSchedule = getConventionEvent().isInSchedule();
        ((ToggleButton) findViewById(R.id.btn_calendar)).setChecked(isInSchedule);
    }

    private void setUpHighlightToolbarState() {
        boolean isHighlighted = getConventionEvent().isHighlighted();
        ((ToggleButton) findViewById(R.id.btn_highlighter)).setChecked(isHighlighted);
    }

    private void setUpYayToolbarState() {
        boolean yay = getConventionEvent().isYay();
        ((ToggleButton) findViewById(R.id.btn_yay)).setChecked(yay);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpToolbarState();
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, getConventionEvent().getExternalId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, getConventionEvent().getFullTitleNoHtml());
        this.firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);
    }

	@Override
	protected void onDestroy() {
		getEventService().removePropertyChangeListener(this);
		super.onDestroy();
	}

	void sendAnalyticsEvent(String name, String eventTitle) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, getConventionEvent().getExternalId());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, eventTitle);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, name);
        this.firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public void toggleHighlight(boolean highlight) {
        Event event = getConventionEvent();
        event.setHighlighted(highlight);
        sendAnalyticsEvent(
                highlight ? "Highlight event" : "Unhighlight event",
                event.getFullTitleNoHtml());
        getEventService().saveEvent(event);
        setUpHighlightToolbarState();
    }

    public void toggleScheduled(boolean highlight) {
        Event event = getConventionEvent();
        if (highlight != event.isInSchedule()) {
            event.setInSchedule(highlight);
            sendAnalyticsEvent(
                    highlight ? "Add to schedule" : "Remove from schedule",
                    event.getFullTitleNoHtml());
            getEventService().saveEvent(event);
        }
        setUpScheduledToolbarState();
    }

    public void toggleYay(boolean highlight) {
        Event event = getConventionEvent();
        if (highlight != event.isYay()) {
            event.setYay(highlight);
            int ranking = event.getRanking().getYayCount();
            event.getRanking().setYayCount(highlight ? ranking + 1 : Math.max(0, ranking - 1));
            sendAnalyticsEvent(
                    highlight ? "Yay" : "Remove Yay",
                    event.getFullTitleNoHtml());
            getEventService().saveEvent(event);
        }
        setUpYayToolbarState();
    }

	private Event getConventionEvent() {
        String participantId = getIntent().getStringExtra("ENTITY_ID");
        EntityType entityType = (EntityType) getIntent().getSerializableExtra("ENTITY_TYPE");
        Renderable renderable = getEventService().getEntityById(entityType, participantId);
        return (Event) renderable;
	}

	private EventService getEventService() {
		BaseEventApplication application = (BaseEventApplication) getApplication();
		return application.getEventService();
	}


    private void populateContents() {
        final Event event = getConventionEvent();

        ImageView icon = findViewById(R.id.panel_type_icon);
        icon.setImageResource(EventListAdapter.getEventIconId(event));

        TextView title = findViewById(R.id.title);
        title.setText(Html.fromHtml(event.getFullTitle()));

        ImageView map = findViewById(R.id.map);
        if (event.getLocation().getThumbnail() != null) {
            map.setImageDrawable(imageFromLocation(event.getLocation()));
            map.setVisibility(View.VISIBLE);
        } else {
            map.setVisibility(View.GONE);
        }

        TextView bio = findViewById(R.id.description);
        if (event.getDescription() != null && event.getDescription().trim().length() > 0) {
            bio.setText(Html.fromHtml(event.getDescription()));
            bio.setVisibility(View.VISIBLE);
        } else {
            bio.setVisibility(View.GONE);
        }

        if (event.isCancelled()) {
            findViewById(R.id.cancelled_message).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.cancelled_message).setVisibility(View.GONE);
        }

        LinearLayout linksLayout = findViewById(R.id.link_list);
        linksLayout.removeAllViews();
        if (event.getLinks().isEmpty()) {
            linksLayout.setVisibility(View.GONE);
        } else {
            linksLayout.setVisibility(View.VISIBLE);
            for (final Link link: event.getLinks()) {
                View view = getLayoutInflater().inflate(R.layout.simple_item_layout, null);
                ImageView image = view.findViewById(R.id.icon);
                image.setImageResource(link.getType().getIcon());

                TextView textView = view.findViewById(R.id.label);
                textView.setText(Html.fromHtml(link.getName()));
                textView.setSingleLine();
                textView.setEllipsize(TextUtils.TruncateAt.END);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openLink(link);
                    }
                });
                linksLayout.addView(view);
            }
        }

        TextView info = findViewById(R.id.info_text);
        String html = event.getSubTitle();
        if (event.getTrackCategory() != null && event.getTrackCategory().length() > 0) {
            html = event.getTrackCategory() + "<br>" + html;
        }

        info.setText(Html.fromHtml(html));

        TextView hashtag = findViewById(R.id.hashtag);
        if (event.getHashTag() != null && event.getHashTag().length() > 0) {
            hashtag.setText(event.getHashTag());
            hashtag.setVisibility(View.VISIBLE);
        } else {
            hashtag.setVisibility(View.GONE);
        }

        View button = findViewById(R.id.tweet_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String tweetUrl = "https://twitter.com/intent/tweet";
                    if (event.getHashTag() != null && event.getHashTag().length() > 0) {
                        tweetUrl = "https://twitter.com/intent/tweet?text=" + URLEncoder.encode(event.getHashTag(), "UTF-8");
                    }
                    Uri uri = Uri.parse(tweetUrl);
                    startActivity(new Intent(Intent.ACTION_VIEW, uri));
                } catch (UnsupportedEncodingException e) {
                }
            }
        });

        LinearLayout layout = findViewById(R.id.event_list);
        layout.removeAllViews();
        if (event.getSubEvents() != null) {
            layout.setVisibility(View.VISIBLE);
            ProgramItemPopulator populator = new ProgramItemPopulator((PersonIdProvider) getApplication(), null);
            for (final Event subEvent : event.getSubEvents()) {
                View view = getLayoutInflater().inflate(R.layout.program_item_layout, null);
                populator.populateProgramListItem(view, subEvent, getResources());
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //view.playSoundEffect(SoundEffectConstants.CLICK);
                        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                        new EventOpener(EventDetailsActivity.this).open(subEvent);
                    }
                });
                layout.addView(view);
            }
        } else {
            layout.setVisibility(View.GONE);
        }

        LinearLayout participantsList = findViewById(R.id.participants_list);
        participantsList.removeAllViews();
        if (event.getParticipants() != null) {
            participantsList.setVisibility(View.VISIBLE);
            ParticpantPopulator populator = new ParticpantPopulator((PersonIdProvider) getApplication(), ((FogGuideApplication) getApplication()).getImageService());
            for (final Participation participation : event.getParticipants()) {
                View view = getLayoutInflater().inflate(R.layout.participant_list_item, null);
                populator.populateParticipantListItem(view, participation, getResources());
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                        new ParticipantItemOpener(EventDetailsActivity.this).open(participation.getParticipant());
                    }
                });
                participantsList.addView(view);
            }
        } else {
            participantsList.setVisibility(View.GONE);
        }

        adjustViewsToReflectEventState();
    }

    private void adjustViewsToReflectEventState() {
        final Event event = getConventionEvent();
        View details = findViewById(R.id.detail_contents);
	    if (event.isHighlighted()) {
	        details.setBackgroundColor(Color.YELLOW);
        } else {
	        details.setBackgroundColor(getResources().getColor(R.color.translucentWhite));
        }

        if (event.getRanking() != null) {
            TextView yayBadge = findViewById(R.id.badge_yay);
	        if (event.getRanking().getYayCount() > 0) {
	            yayBadge.setVisibility(View.VISIBLE);
	            yayBadge.setText("" + event.getRanking().getYayCount());
	            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
	                yayBadge.setElevation(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
                } else {
                    yayBadge.bringToFront();
                }
            } else {
                yayBadge.setVisibility(View.GONE);
            }
        }
    }

    private void openLink(Link link) {
	    if (link.getHref() != null && link.getHref().startsWith("wissched:conInfo/")) {

        } else {
            try {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getHref()));
                startActivity(myIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(EventDetailsActivity.this, "Oh dear. Horrible horrible things have happened",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
	public void propertyChange(PropertyChangeEvent event) {
		if (getConventionEvent() == event.getSource()) {
			runOnUiThread(new Runnable() {
			     @Override
			     public void run() {
			    	 populateContents();
			     }
			});
		}
	}

    Drawable imageFromLocation(Location location) {
        try {
            InputStream ims = getAssets().open(location.getThumbnail());
            return Drawable.createFromStream(ims, null);
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
