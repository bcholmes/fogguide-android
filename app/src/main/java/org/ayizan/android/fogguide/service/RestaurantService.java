package org.ayizan.android.fogguide.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.util.JSONResourceReader;

import android.content.Context;

public class RestaurantService {

	private JSONResourceReader reader = new JSONResourceReader();
	private boolean initialized = false;
	private Map<String,Restaurant> restaurants = new HashMap<String,Restaurant>();
	private Context context;

	public RestaurantService(Context applicationContext) {
		this.context = applicationContext;
	}

	protected synchronized void initialize() {
		/*
		if (!this.initialized) {
			try {
				JSONObject json = this.reader.loadJson(this.context, "restaurants.json");

				JSONArray list = json.getJSONArray("restaurants");
				for (int i = 0; i < list.length(); i++) {
					JSONObject restaurantJson = list.getJSONObject(i);
					Restaurant restaurant = Restaurant.fromJson(restaurantJson);
					this.restaurants.put(restaurant.getId(), restaurant);
					
				}
				this.initialized = true;
			} catch (JSONException e) {
				Log.w("WisSched", "JSON restaurant file parse error");
			}
		}
		*/
	}

	public List<Restaurant> getRestaurants() {
		if (!this.initialized) {
			initialize();
		}
		List<Restaurant> restaurants = new ArrayList<Restaurant>(this.restaurants.values());
		Collections.sort(restaurants);
		return restaurants;
	}

	public Renderable getRestaurantById(String entityId) {
		return this.restaurants.get(entityId);
	}
}
