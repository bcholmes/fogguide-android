package org.ayizan.android.fogguide.view.grid;

/**
 * Created by bcholmes on 2018-02-23.
 */

public interface ColumnHeaderProvider {
    String getHeaderLabel(int columnNumber);
}
