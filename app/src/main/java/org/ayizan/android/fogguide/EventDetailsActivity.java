package org.ayizan.android.fogguide;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.service.EventService;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class EventDetailsActivity extends DetailsActivity implements PropertyChangeListener {
	
	private Menu menu;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getEventService().addPropertyChangeListener(this);
	}

	@Override
	protected void onDestroy() {
		getEventService().removePropertyChangeListener(this);
		super.onDestroy();
	}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_bio_details, menu);
        this.menu = menu;
        return true;
    }

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem highlight = menu.findItem(R.id.menu_highlight);
		MenuItem unhighlight = menu.findItem(R.id.menu_unhighlight);

		MenuItem scheduleAdd = menu.findItem(R.id.menu_add_to_schedule);
		MenuItem scheduleRemove = menu.findItem(R.id.menu_remove_from_schedule);
		
		boolean isHighlighted = getConventionEvent().isHighlighted();
		highlight.setVisible(!isHighlighted);
		unhighlight.setVisible(isHighlighted);
		
		boolean isInSchedule = getConventionEvent().isInSchedule();
		
		scheduleAdd.setVisible(!isInSchedule);
		scheduleRemove.setVisible(isInSchedule);
		
		MenuItem yay = menu.findItem(R.id.menu_yay);
		MenuItem unyay = menu.findItem(R.id.menu_remove_yay);
		
		yay.setVisible(!getConventionEvent().isYay());
		unyay.setVisible(getConventionEvent().isYay());
		
		MenuItem omg = menu.findItem(R.id.menu_omg);
		MenuItem unomg = menu.findItem(R.id.menu_remove_omg);
		
		omg.setVisible(!getConventionEvent().isOmg());
		unomg.setVisible(getConventionEvent().isOmg());
		return true;
	}
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_highlight:
            case R.id.menu_unhighlight: {
	            	Event event = getConventionEvent();
	            	event.setHighlighted(item.getItemId() == R.id.menu_highlight);
	            	getEventService().saveEvent(event);
	            }
            	return true;
            case R.id.menu_add_to_schedule:
            case R.id.menu_remove_from_schedule: {
	            	Event event = getConventionEvent();
	            	event.setInSchedule(item.getItemId() == R.id.menu_add_to_schedule);
	            	getEventService().saveEvent(event);
	            }
	            return true;
            case R.id.menu_yay:
            case R.id.menu_remove_yay: {
	            	Event event = getConventionEvent();
	            	event.setYay(item.getItemId() == R.id.menu_yay);
	            	int delta = item.getItemId() == R.id.menu_yay ? 1 : -1;
	            	if (event.getRanking().getYayCount() + delta >= 0) {
	            		event.getRanking().setYayCount(event.getRanking().getYayCount() + delta);
	            	}

	            	getEventService().saveEvent(event);
	            }
	            return true;
            case R.id.menu_omg:
            case R.id.menu_remove_omg: {
	            	Event event = getConventionEvent();
	            	event.setOmg(item.getItemId() == R.id.menu_omg);
	            	getEventService().saveEvent(event);
	            }
	            return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public String getHashTag() {
    	return getConventionEvent().getHashTag();
    }

	private Event getConventionEvent() {
        String participantId = getIntent().getStringExtra("ENTITY_ID");
        EntityType entityType = (EntityType) getIntent().getSerializableExtra("ENTITY_TYPE");
        Renderable renderable = getEventService().getEntityById(entityType, participantId);
        return (Event) renderable;
	}

	private EventService getEventService() {
		FogGuideApplication application = (FogGuideApplication) getApplication();
		return application.getEventService();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if (getConventionEvent() == event.getSource()) {
			runOnUiThread(new Runnable() {
			     @Override
			     public void run() {
			    	 refreshWebView();
			     }
			});
		}
	}    
}
