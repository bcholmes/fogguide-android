package org.ayizan.android.fogguide.view;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import org.ayizan.android.event.util.view.ItemOpener;

import java.util.List;

/**
 * Created by BC Holmes on 2016-08-18.
 */
abstract class BaseRecordViewAdapter<T> extends RecyclerView.Adapter<BaseRecordViewAdapter.ViewHolder>
        implements View.OnClickListener {

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected View view;

        public ViewHolder(View v, View.OnClickListener listener) {
            super(v);
            this.view = v;
            this.view.setOnClickListener(listener);
        }
    }

    final RecyclerView recyclerView;
    List<T> records;
    final ItemOpener<T> opener;

    public BaseRecordViewAdapter(RecyclerView recyclerView, List<T> records,
                                 ItemOpener<T> opener) {
        this.recyclerView = recyclerView;
        this.records = records;
        this.opener = opener;
    }

    T getRecord(int position) {
        return this.records.get(position);
    }

    @Override
    public int getItemCount() {
        return this.records.size();
    }

    @Override
    public void onClick(View view) {
        int position = this.recyclerView.getChildLayoutPosition(view);
        T record = getRecord(position);
        this.opener.open(record);
    }
}
