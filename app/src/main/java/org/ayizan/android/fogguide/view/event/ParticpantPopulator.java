package org.ayizan.android.fogguide.view.event;

import android.content.res.Resources;
import android.graphics.Paint;
import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.model.Participation;
import org.ayizan.android.event.service.ImageService;
import org.ayizan.android.event.util.PersonIdProvider;
import org.ayizan.android.event.util.StringUtils;
import org.ayizan.android.fogguide.R;

public class ParticpantPopulator {

    private PersonIdProvider personIdProvider;
    private ImageService imageService;

    public ParticpantPopulator(PersonIdProvider personIdProvider, ImageService imageService) {
        this.personIdProvider = personIdProvider;
        this.imageService = imageService;
    }

    public void populateParticipantListItem(View view, Participation participation, Resources resources) {
        ImageView icon = view.findViewById(R.id.avatar_icon);

        Participant participant = participation.getParticipant();
        if (!participant.isAnonymous() && this.imageService.isAvatarAvailable(participant)) {
            icon.setImageURI(Uri.fromFile(this.imageService.fileForParticipant(participant)));
        } else {
            icon.setImageResource(participant.getDefaultIcon());
        }

        TextView pronounLabel = view.findViewById(R.id.sub_label);
        if (StringUtils.isNotEmpty(participant.getPronouns())) {
            pronounLabel.setText(participant.getPronouns());
            pronounLabel.setVisibility(View.VISIBLE);
        } else {
            pronounLabel.setVisibility(View.GONE);
        }
        TextView textView = (TextView) view.findViewById(R.id.label);
        textView.setText(Html.fromHtml(participation.getParticipant().getFullName()));
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);

        if (participation.isCancelled() || participant.isCancelled()) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(0xffbbbbbb);
        } else {
            textView.setPaintFlags(textView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(0x8a000000);
        }
    }
}
