package org.ayizan.android.fogguide.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import org.ayizan.android.event.model.Groupable;
import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.util.RenderContext;
import org.ayizan.android.event.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;

public class Restaurant implements Groupable, Comparable<Restaurant>, Renderable {
	
	private String id;
	private String name;
	private String url;
	private String cuisine;
	private String streetAddress;
	private String fullAddress;
	private String notes;
	private String hours;
	private String memorialDayStatus;
	private String veggie;
	private String allergen;
	private String alcohol;
	private String takeout;
	private String phoneNumber;
	private boolean wifi;
	private boolean delivery;
	private boolean saladFlag;
	private boolean soupFlag;
	private int priceRange;
	private double latitude;
	private double longitude;
	private boolean reservationRecommended;

	public static Restaurant fromJson(JSONObject restaurantJson) throws JSONException {
		Restaurant result = new Restaurant();
		if (restaurantJson.has("id")) {
			result.id = restaurantJson.getString("id");
		}
		if (restaurantJson.has("name")) {
			result.name = restaurantJson.getString("name");
		}
		if (restaurantJson.has("url")) {
			result.url = restaurantJson.getString("url");
		}
		if (restaurantJson.has("hours")) {
			result.hours = restaurantJson.getString("hours");
		}
		if (restaurantJson.has("alcohol")) {
			result.alcohol = restaurantJson.getString("alcohol");
		}
		if (restaurantJson.has("cuisine")) {
			result.cuisine = restaurantJson.getString("cuisine");
		}
		if (restaurantJson.has("streetAddress")) {
			result.streetAddress = restaurantJson.getString("streetAddress");
		}
		if (restaurantJson.has("fullAddress")) {
			result.fullAddress = restaurantJson.getString("fullAddress");
		}
		if (restaurantJson.has("notes")) {
			result.notes = restaurantJson.getString("notes");
		}
		if (restaurantJson.has("memorialDayStatus")) {
			result.memorialDayStatus = restaurantJson.getString("memorialDayStatus");
		}
		if (restaurantJson.has("veggie")) {
			result.veggie = restaurantJson.getString("veggie");
		}
		if (restaurantJson.has("allergen")) {
			result.allergen = restaurantJson.getString("allergen");
		}
		if (restaurantJson.has("takeout")) {
			result.takeout = restaurantJson.getString("takeout");
		}
		if (restaurantJson.has("phoneNumber")) {
			result.phoneNumber = restaurantJson.getString("phoneNumber");
		}
		if (restaurantJson.has("wifi")) {
			result.wifi = restaurantJson.getBoolean("wifi");
		}
		if (restaurantJson.has("delivery")) {
			result.delivery = restaurantJson.getBoolean("delivery");
		}
		if (restaurantJson.has("saladFlag")) {
			result.saladFlag = restaurantJson.getBoolean("saladFlag");
		}
		if (restaurantJson.has("soupFlag")) {
			result.soupFlag = restaurantJson.getBoolean("soupFlag");
		}
		if (restaurantJson.has("reservationRecommended")) {
			result.reservationRecommended = restaurantJson.getBoolean("reservationRecommended");
		}
		if (restaurantJson.has("priceRange")) {
			result.priceRange = restaurantJson.getInt("priceRange");
		}
		if (restaurantJson.has("latitude")) {
			result.latitude = restaurantJson.getDouble("latitude");
		}
		if (restaurantJson.has("longitude")) {
			result.longitude = restaurantJson.getDouble("longitude");
		}
		return result;
	}

	public String getName() {
		return name == null ? "" : name;
	}

	public String getUrl() {
		return url;
	}

	public String getCuisine() {
		return cuisine;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public String getNotes() {
		return notes;
	}

	public String getHours() {
		return hours;
	}

	public String getMemorialDayStatus() {
		return memorialDayStatus;
	}

	public String getVeggie() {
		return veggie;
	}

	public String getAllergen() {
		return allergen;
	}

	public String getTakeout() {
		return takeout;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public boolean isWifi() {
		return wifi;
	}

	public boolean isDelivery() {
		return delivery;
	}

	public boolean isSaladFlag() {
		return saladFlag;
	}

	public int getPriceRange() {
		return priceRange;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public boolean isReservationRecommended() {
		return reservationRecommended;
	}

	public boolean isSoupFlag() {
		return soupFlag;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public String getGroup() {
		if (StringUtils.isNotEmpty(getName())) {
			String firstCharacter = getName().toUpperCase(Locale.ENGLISH).substring(0, 1);
			return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(firstCharacter) >= 0 ? firstCharacter : "#";
		} else {
			return "?";
		}
	}

	public String getId() {
		return id;
	}
	public String toString() {
		return getName();
	}

	@Override
	public int compareTo(Restaurant another) {
		return getName().compareTo(another.getName());
	}

	@Override
	public String getFullHtml(RenderContext renderContext) {
		StringBuilder builder = new StringBuilder();
		builder.append("<html><head><title>").append(getName())
			.append("%@</title><link rel=\"stylesheet\" type=\"text/css\" href=\"./list.css\" />");
		builder.append("<meta name=\"format-detection\" content=\"telephone=no\">");
		builder.append("</head><body><div class=\"content\"><div class=\"main\">");
		builder.append("<p><b>").append(getName()).append("</b></p>");
		if (!StringUtils.isBlank(this.fullAddress)) {
			try {
				String mapUrl = String.format("geo:0,0?q=%s",URLEncoder.encode(this.fullAddress, "UTF-8"));
				builder.append("<div class=\"floatRight\"><a class=\"map-button\" href=\"").append(mapUrl).append("\">Map</a></div>");
			} catch (UnsupportedEncodingException e) {
			}
		}
		builder.append("<p>").append(getStreetAddress()).append("<br />");
		builder.append(getPhoneNumber()).append("</p>");
		
		builder.append("<p><b>Cuisine:</b> ").append(getCuisine()).append("<br />");
		builder.append("<b>Veggie entrees:</b> ").append(getVeggie()).append("<br />");
		builder.append("<b>Salad?</b> ").append(isSaladFlag() ? "Yes" : "No").append("<br />");
		builder.append("<b>Soup?</b> ").append(isSoupFlag() ? "Yes" : "No").append("<br />");
		builder.append("<b>Alcohol:</b> ").append(getAlcohol()).append("<br />");
		builder.append("<b>Allergen info:</b> ").append(getAllergen());
		builder.append("</p>");
		
		builder.append("<p><b>Take-out:</b> ").append(getTakeout()).append("<br />");
		builder.append("<b>Delivery:</b> ").append(isDelivery() ? "Yes" : "No");
		builder.append("</p>");
		
		builder.append("<p><b>Wifi:</b> ").append(isWifi() ? "Yes" : "").append("</p>");
		
		builder.append("<p><b>Hours:</b> ").append(getHours()).append("<br />");
		builder.append("<b>Open Memorial Day?</b> ").append(getMemorialDayStatus()).append("<br />");
		builder.append("<b>URL:</b> ").append(getUrl());
		builder.append("</p>");
		
		if (StringUtils.isNotEmpty(this.notes)) {
			builder.append("<p><b>Notes:</b><br />").append(this.notes).append("</p>");
		}
		
		if (isReservationRecommended()) {
			builder.append("<p class=\"notice\">Reservations recommended</p>");
		}
		builder.append("</div></div></body></html>");
		return builder.toString();
	}

	@Override
	public String getFullTitleNoHtml() {
		return getName();
	}
	
	public String getAnnotations() {
		StringBuilder builder = new StringBuilder();
		
		if (!StringUtils.isBlank(getCuisine())) {
			builder.append(getCuisine());
		}
		if (this.priceRange >= 1 && this.priceRange <= 3) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			for (int i = 0; i < this.priceRange; i++) {
				builder.append("$");
			}
		}
		
		if (!"No".equals(getTakeout())) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append("Take-out");
		}
		if (isDelivery()) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append("Delivery");
		}
		if (!StringUtils.isBlank(getVeggie()) && !"no".equalsIgnoreCase(getVeggie())) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append("Veggie (").append(getVeggie()).append(")");
		}
		if (!StringUtils.isBlank(getAlcohol()) && !"no".equalsIgnoreCase(getAlcohol())) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(getAlcohol());
		}
		if (isWifi()) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append("Wifi");
		}
		if (!StringUtils.isBlank(getAllergen())) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(getAllergen());
		}
		
		return builder.toString();
	}

	public String getAlcohol() {
		return alcohol;
	}

	public String getFullAddress() {
		return fullAddress;
	}
	
}
