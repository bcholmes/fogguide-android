package org.ayizan.android.fogguide.service;

import android.content.Context;

import org.ayizan.android.event.model.Announcement;

import java.util.Collections;
import java.util.List;

public class AnnouncementService {

	private Context context;
	private List<Announcement> announcements = Collections.<Announcement>emptyList();

	/*
	class AnnouncementFetchTask extends HttpRequestTask {


		protected AnnouncementFetchTask(Context context) {
			super(context);
		}

		@Override
		protected boolean convertToJsonAndProcessResult(HttpResponse response)
				throws IOException, JSONException {
			InputStream input = response.getEntity().getContent();
			JSONArray json = new JSONArray(IOUtils.toString(input, "UTF-8"));
			return processJsonArray(json);			
		}
		
		private boolean processJsonArray(JSONArray json) throws JSONException {
			AnnouncementService.this.announcements = Announcement.fromJsonCollection(json);
			return false;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			try {
				Log.d("WisSched", "Fetch annouoncements");
				HttpGet postMethod = new HttpGet("http://blog.wiscon.info/wp-json/posts");
				addCommonHeaders(postMethod);
		        HttpResponse response = this.client.execute(postMethod);
				
		        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
		        	processResponse(response);
		        }
		        return true;
			} catch (IOException e) {
				Log.e("WisSched", "Unable to download announcements", e);
				return false;
			}
		}
	}
	*/
	
	public AnnouncementService(Context context) {
		this.context = context;
	}

	public List<Announcement> getAnnouncements() {
		return announcements;
	}

	public void fetchLatestUpdates() {
/*		new AnnouncementFetchTask(this.context).execute(); */
	}

	public Announcement getAnnouncementById(String entityId) {
		Announcement result = null;
		for (Announcement announcement : this.announcements) {
			if (entityId.equals(announcement.getId())) {
				result = announcement;
				break;
			}
		}
		return result;
	}
}
