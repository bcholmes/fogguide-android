package org.ayizan.android.fogguide.service;

public class ReferenceItem {
	
	private String description;
	private String resourceName;
	private ReferenceItem[] items;
	
	public ReferenceItem(String description, String resourceName) {
		this.description = description;
		this.resourceName = resourceName;
	}
	public ReferenceItem(String description, ReferenceItem[] items) {
		this.description = description;
		this.items = items;
	}
	public String getDescription() {
		return description;
	}
	public String getResourceName() {
		return resourceName;
	}
	public String toString() {
		return getDescription();
	}
	public boolean isSubMenu() {
		return this.items != null;
	}
	public ReferenceItem[] getItems() {
		return items;
	}
}
