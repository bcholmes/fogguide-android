package org.ayizan.android.fogguide;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.SoundEffectConstants;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import org.ayizan.android.event.model.EntityType;

final class FogGuideWebViewClient extends WebViewClient {
	
	static class TrivialHashTagProvider implements HashTagProvider {
		@Override
		public String getHashTag() {
			return "";
		}
	}
	
	private final Activity activity;
	private HashTagProvider hashTagProvider;

	FogGuideWebViewClient(Activity activity, HashTagProvider hashTagProvider) {
		this.activity = activity;
		this.hashTagProvider = hashTagProvider != null ? hashTagProvider : new TrivialHashTagProvider();
	}

	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		if (url.contains("/device/imagepicker/")) {
			
			Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			activity.startActivityForResult(i, DetailsActivity.RESULT_LOAD_IMAGE);
			
			return true;
		} else if (url.toLowerCase().startsWith("http:") || url.toLowerCase().startsWith("https:") || url.toLowerCase().startsWith("mailto:")) {
			try {
			    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			    activity.startActivity(myIntent);
			} catch (ActivityNotFoundException e) {
			    Toast.makeText(activity, "Oh dear. Horrible horrible things have happened",  
			    		Toast.LENGTH_LONG).show();
			}
			return true;
		} else if (url.startsWith("geo:")) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			activity.startActivity(intent);
			return true;
		} else if (url.contains("/tweet/")) {
			try {
				String tweetUrl = "https://twitter.com/intent/tweet?text=" + URLEncoder.encode(this.hashTagProvider.getHashTag(), "UTF-8");
			    Uri uri = Uri.parse(tweetUrl);
			    activity.startActivity(new Intent(Intent.ACTION_VIEW, uri));
			} catch (UnsupportedEncodingException e) {
			}
			return true;
		} else {
			return false;
		}
	}
}