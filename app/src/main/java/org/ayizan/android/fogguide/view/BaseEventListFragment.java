package org.ayizan.android.fogguide.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.service.EventService;
import org.ayizan.android.event.util.PersonIdProvider;
import org.ayizan.android.fogguide.R;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import ca.barrenechea.widget.recyclerview.decoration.DividerDecoration;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;

/**
 * Created by bcholmes on 2017-05-19.
 */
public abstract class BaseEventListFragment extends Fragment implements PropertyChangeListener, PersonIdProvider {

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    EventListAdapter adapter;
    StickyHeaderDecoration headerDecoration;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.simple_recycler_layout, container, false);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.simple_list);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final DividerDecoration divider = new DividerDecoration.Builder(this.getActivity())
                .setHeight(R.dimen.default_divider_height)
                .setPadding(R.dimen.default_divider_padding)
                .setColorResource(R.color.lightGray)
                .build();

        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(this.layoutManager = new LinearLayoutManager(this.getActivity()));
        this.recyclerView.addItemDecoration(divider);

        getEventService().addPropertyChangeListener(this);

        List<Event> events = getEvents();
        this.adapter = new EventListAdapter(this.recyclerView, events, new EventOpener(getActivity()), this);
        this.headerDecoration = new StickyHeaderDecoration(adapter);
        setHasOptionsMenu(true);

        this.recyclerView.setAdapter(this.adapter);
        this.recyclerView.addItemDecoration(this.headerDecoration, 1);
    }

    protected abstract List<Event> getEvents();

    protected EventService getEventService() {
        BaseEventApplication application = (BaseEventApplication) getActivity().getApplication();
        return application.getEventService();
    }

    @Override
    public void onDestroy() {
        getEventService().removePropertyChangeListener(this);
        super.onDestroy();
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if ("event".equals(event.getPropertyName())) {
            Event e = (Event) event.getNewValue();
            int index = this.adapter.sortedList.indexOf(e);
            if (index >= 0) {
                this.adapter.notifyItemChanged(index);
            }
        } else if ("events".equals(event.getPropertyName())) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    BaseEventListFragment.this.adapter.replaceAll(getEvents());
                    BaseEventListFragment.this.headerDecoration.clearHeaderCache();;
                }
            });
        }
    }

    public String getPersonId() {
        PersonIdProvider application = (PersonIdProvider) getActivity().getApplication();
        return application.getPersonId();
    }
}
