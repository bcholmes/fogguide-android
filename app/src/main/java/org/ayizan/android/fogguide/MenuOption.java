package org.ayizan.android.fogguide;

import androidx.fragment.app.Fragment;

import org.ayizan.android.event.util.WordUtils;
import org.ayizan.android.fogguide.view.LeaderboardFragment;
import org.ayizan.android.fogguide.view.MyScheduleFragment;
import org.ayizan.android.fogguide.view.ParticipantsFragment;
import org.ayizan.android.fogguide.view.ProgramFragment;
import org.ayizan.android.fogguide.view.grid.GridScheduleFragment;

public enum MenuOption {
	PROGRAM(ProgramFragment.class),
	MY_SCHEDULE(MyScheduleFragment.class),
	BIOS(ParticipantsFragment.class),
	GRID_SCHEDULE(GridScheduleFragment.class),
//	CON_INFO(ReferenceActivity.class),
//	RESTAURANTS(RestaurantListFragment.class),
//	ANNOUNCEMENTS(AnnouncementsFragment.class),
	LEADERBOARD(LeaderboardFragment.class)
//	,
//	LOGIN(null)
	;

	private Class<? extends Fragment> fragmentClass;
	
	private MenuOption(Class<? extends Fragment> fragmentClass) {
		this.fragmentClass = fragmentClass;
	}
	public String toString() {
		return WordUtils.capitalizeFully(name().replace('_', ' '));
	}
	public Class<? extends Fragment> getFragmentClass() {
		return this.fragmentClass;
	}
}
