package org.ayizan.android.fogguide.service;

public interface LoginResult {

	public void loginSuccess();
	public void loginFailed();
	
}
