package org.ayizan.android.fogguide.view.grid;

import android.graphics.Shader;

import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.model.Location;
import org.ayizan.android.event.model.ScheduledItem;
import org.ayizan.android.event.util.DateFlyweight;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by bcholmes on 2018-02-20.
 */
public class GridViewEvent {

    int columnNumber;
    ScheduledItem item;
    int span;

    public GridViewEvent(ScheduledItem item, int columnNumber) {
        this.item = item;
        this.columnNumber = columnNumber;
        this.span = item.getLocation().getAllLocations().size();
    }

    public Calendar getStartTime() {
        Calendar result = new GregorianCalendar(DateFlyweight.timeZone);
        result.setTime(this.item.getTimeRange().getStartTime());
        return result;
    }

    public Calendar getEndTime() {
        Calendar result = new GregorianCalendar(DateFlyweight.timeZone);
        result.setTime(this.item.getTimeRange().getEndTime());
        return result;
    }

    public String getIdentifier() {
        return this.item.getFullIdentifier();
    }

    public String getName() {
        return this.item.getFullTitleNoHtml();
    }

    public String getLocation() {
        return this.item.getLocation().getDisplayName();
    }

    public String getSubTitle() {
        return (this.item instanceof Event) ? ((Event) this.item).getTrackCategory() : null;
    }

    public int getColor() {
        return 0;
    }

    public Shader getShader() {
        return null;
    }

    public int getColumnNumber() {
        return columnNumber;
    }

    public int getSpan() {
        return this.span;
    }

    public boolean isInLocation(Location location) {
        return this.item.getLocation().getAllLocations().contains(location);
    }

    public boolean intersects(int startColumn, int endColumn) {

        return (this.columnNumber >= startColumn && this.columnNumber <= endColumn) ||
                ((this.columnNumber+this.span) >= startColumn && (this.columnNumber+this.span) <= endColumn) ||
                (this.columnNumber < startColumn && (this.columnNumber + this.span) > endColumn);
    }
}
