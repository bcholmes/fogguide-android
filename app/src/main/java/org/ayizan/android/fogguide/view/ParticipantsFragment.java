package org.ayizan.android.fogguide.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.ayizan.android.event.service.EventService;
import org.ayizan.android.fogguide.FogGuideApplication;
import org.ayizan.android.fogguide.R;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import ca.barrenechea.widget.recyclerview.decoration.DividerDecoration;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;

public class ParticipantsFragment extends Fragment implements PropertyChangeListener  {
	
	private RecyclerView recyclerView;
	private ParticipantListAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.simple_recycler_layout, container, false);
		this.recyclerView = (RecyclerView) view.findViewById(R.id.simple_list);
		return view;
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		final DividerDecoration divider = new DividerDecoration.Builder(this.getActivity())
				.setHeight(R.dimen.default_divider_height)
				.setPadding(R.dimen.default_divider_padding)
				.setColorResource(R.color.lightGray)
				.build();

		this.recyclerView.setHasFixedSize(true);
		this.recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
		this.recyclerView.addItemDecoration(divider);

		setAdapterAndDecoration(this.recyclerView);
	}

	protected void setAdapterAndDecoration(RecyclerView list) {
		this.adapter = new ParticipantListAdapter(this.getActivity(),
				this.recyclerView, getFogGuideApplication().getImageService(),
				getEventService().getParticipants(), new ParticipantItemOpener(getActivity()));
		StickyHeaderDecoration decoration = new StickyHeaderDecoration(adapter);
		setHasOptionsMenu(true);

		list.setAdapter(this.adapter);
		list.addItemDecoration(decoration, 1);
	}

	private EventService getEventService() {
		FogGuideApplication application = getFogGuideApplication();
		return application.getEventService();
	}

	private FogGuideApplication getFogGuideApplication() {
		return (FogGuideApplication) getActivity().getApplication();
	}
	
	@Override
	public void onDestroy() {
		getEventService().removePropertyChangeListener(this);
		super.onDestroy();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if ("events".equals(event.getPropertyName())) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					ParticipantsFragment.this.adapter.setParticipants(getEventService().getParticipants());
					ParticipantsFragment.this.adapter.notifyDataSetChanged();
				}
			});
		}
	}
}
