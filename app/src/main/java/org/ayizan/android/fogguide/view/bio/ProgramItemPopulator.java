package org.ayizan.android.fogguide.view.bio;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.model.Participation;
import org.ayizan.android.event.util.PersonIdProvider;
import org.ayizan.android.fogguide.R;
import org.ayizan.android.fogguide.view.EventListAdapter;

/**
 * Created by bcholmes on 2018-03-10.
 */

public class ProgramItemPopulator {

    private PersonIdProvider personIdProvider;
    private Participant participant;

    public ProgramItemPopulator(PersonIdProvider personIdProvider, Participant participant) {
        this.personIdProvider = personIdProvider;
        this.participant = participant;
    }

    public void populateProgramListItem(View view, Event event, Resources resources) {
        ImageView icon = view.findViewById(R.id.panel_type_icon);
        icon.setImageResource(EventListAdapter.getEventIconId(event));

        TextView textView = (TextView) view.findViewById(R.id.label);
        textView.setText(Html.fromHtml(event.getFullTitle()));
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);
        Participation participation = this.participant == null ? null : event.getParticipation(participant);
        if (event.isCancelled() || (participation != null && participation.isCancelled())) {
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(0xffbbbbbb);
        } else {
            textView.setPaintFlags(textView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(0x8a000000);
        }

        TextView subLabel = (TextView) view.findViewById(R.id.sub_label);
        subLabel.setText(event.getLocationAndTrack());
        subLabel.setSingleLine();
        subLabel.setEllipsize(TextUtils.TruncateAt.END);

        assignBackgroundColor(view, event, resources);
    }

    protected void assignBackgroundColor(View view, Event event, Resources resources) {
        if (event.isHighlighted()) {
            view.setBackgroundColor(Color.YELLOW);
        } else if (this.personIdProvider != null  && event.isMyEvent(this.personIdProvider)) {
            view.setBackgroundColor(Color.rgb(209, 212, 239));
        } else {
            view.setBackgroundColor(resources.getColor(R.color.translucentWhite));
        }
    }
}
