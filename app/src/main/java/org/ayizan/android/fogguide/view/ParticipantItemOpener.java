package org.ayizan.android.fogguide.view;


import android.app.Activity;
import android.content.Intent;

import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.util.view.ItemOpener;
import org.ayizan.android.fogguide.view.bio.ParticipantBioActivity;

public class ParticipantItemOpener implements ItemOpener<Participant> {

    private Activity activity;

    public ParticipantItemOpener(Activity activity) {
        this.activity = activity;
    }

    public void open(Participant participant) {
        Intent intent = new Intent(getActivity(), ParticipantBioActivity.class);
        intent.putExtra("ENTITY_ID", participant.getId());
        intent.putExtra("ENTITY_TYPE", EntityType.PARTICIPANT);
        getActivity().startActivity(intent);
    }

    public Activity getActivity() {
        return activity;
    }
}
