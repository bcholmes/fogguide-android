package org.ayizan.android.fogguide;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.LoginTask;
import org.ayizan.android.event.service.ImageService;
import org.ayizan.android.event.util.ConnectivityUtil;
import org.ayizan.android.event.util.DateFlyweight;
import org.ayizan.android.event.util.StringUtils;
import org.ayizan.android.fogguide.service.AnnouncementService;
import org.ayizan.android.fogguide.service.LoginResult;
import org.ayizan.android.fogguide.service.RestaurantService;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

public class FogGuideApplication extends BaseEventApplication {

	public static String CURRENT_CONVENTION = "fogcon2020";
	public static String DATABASE_NAME = "fogcon.db";

	private static final String TOKEN = "hereistherootoftherootandthebudofthebudandtheskyoftheskyofatreecalledlife";

	private static final long THREE_HOURS = 3L * 60L * 60L * 1000L;
	private static final long SIXTY_MINUTES = 60L * 60L * 1000L;
	
	private long assignmentsCheck = -1;
	PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	private RestaurantService restaurantService;
	
	ImageService imageService;
	private AnnouncementService announcementService;

	long lastDataSynchTime = -1;

	public FogGuideApplication() {
		super("FogGuide", CURRENT_CONVENTION, DATABASE_NAME);
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		DateFlyweight.timeZone = TimeZone.getTimeZone("America/Los_Angeles");

        this.imageService = new ImageService(getApplicationContext(), this, CURRENT_CONVENTION);
		this.restaurantService = new RestaurantService(this);
        this.announcementService = new AnnouncementService(getApplicationContext());

        unloginIfNecessary();
	}
	
	protected void unloginIfNecessary() {
		SharedPreferences sharedPref = getWisSchedSharedPreferences();
		if (!sharedPref.getBoolean("upgradedLogin", false)) {
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putBoolean("upgradedLogin", true);
			editor.remove("personId");
			editor.remove("appKey");
			editor.apply();
		}
	}
	

	protected void saveLoginData(String personId, String appKey) {
		SharedPreferences sharedPref = getWisSchedSharedPreferences();
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("personId", personId);
		editor.putString("appKey", appKey);
		editor.apply();
	}
	
	public void login(String email, String password, LoginResult loginResult) {
		new LoginTask(this, email, password, loginResult).execute();
	}
	public void fetchAssignmentsIfNecessary() {
		if (this.assignmentsCheck < 0 || (System.currentTimeMillis() - this.assignmentsCheck) > THREE_HOURS) {
			fetchAssignments();
		}
	}
	public String getClientId() {
		SharedPreferences preferences = getWisSchedSharedPreferences();
		String clientId = preferences.getString("clientId", null);
		if (StringUtils.isEmpty(clientId)) {
			clientId = UUID.randomUUID().toString();
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("clientId", clientId);
			editor.apply();
		}
		return clientId;
	}
	public String getPersonId() {
		return getWisSchedSharedPreferences().getString("personId", null);
	}
	public boolean isLoggedIn() {
		return getWisSchedSharedPreferences().getString("appKey", null) != null;
	}
	
	protected void fetchAssignments() {
		/*
		String appKey = getWisSchedSharedPreferences().getString("appKey", null);
		if (appKey != null) {
			this.assignmentsCheck = System.currentTimeMillis();
			new AssignmentsTask(this, appKey).execute();
		}
		*/
	}

	private SharedPreferences getWisSchedSharedPreferences() {
		SharedPreferences sharedPref = getSharedPreferences(
		        "FogGuide", Context.MODE_PRIVATE);
		return sharedPref;
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String eventType, PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(eventType, listener);
	}
	
	public RestaurantService getRestaurantService() {
		return restaurantService;
	}

	public List<MenuOption> getMenuOptions() {
//		if (isLoggedIn()) {
			return Arrays.asList(MenuOption.PROGRAM, MenuOption.MY_SCHEDULE, MenuOption.BIOS, 
					// MenuOption.CON_INFO, MenuOption.RESTAURANTS, MenuOption.ANNOUNCEMENTS,
					MenuOption.LEADERBOARD);
//		} else {
//			return Arrays.asList(MenuOption.PROGRAM, MenuOption.MY_SCHEDULE, MenuOption.BIOS,
//					MenuOption.CON_INFO, MenuOption.RESTAURANTS, MenuOption.ANNOUNCEMENTS, MenuOption.LEADERBOARD, MenuOption.LOGIN);
//		}
	}

	public void performDataSynchIfApplicable() {
		if (!ConnectivityUtil.isNetworkAvailable(this)) {
			// skip
		} else if (this.lastDataSynchTime < 0 || ((System.currentTimeMillis() - this.lastDataSynchTime) > SIXTY_MINUTES)) {
			long lastCheck = this.lastDataSynchTime;
			this.lastDataSynchTime = System.currentTimeMillis();

			this.eventService.uploadSelections();
			
			this.eventService.fetchLatestUpdates();
			
			this.eventService.fetchLatestRankings();
			
			if (lastCheck < 0 || ((System.currentTimeMillis() - lastCheck) > THREE_HOURS)) {
				this.announcementService.fetchLatestUpdates();
			}
			
			this.imageService.uploadQueuedImage();

//			new AvatarDownloadTask(this.imageService, getRestService(), getApplicationContext(), CURRENT_CONVENTION).execute();
		}
	}
	
	public String getToken() {
		String temp = "" + getClientId() + "||" + TOKEN;
		if (StringUtils.isNotEmpty(getPersonId())) {
			temp += "||" + getPersonId();
		}
		
		try {
		    MessageDigest md = MessageDigest.getInstance("SHA-256");
	
		    md.update(temp.getBytes());
		    byte[] digest = md.digest();
	
		    return Base64.encodeToString(digest, Base64.NO_WRAP);		
		} catch (NoSuchAlgorithmException e) {
			return "";
		}
	}

	public ImageService getImageService() {
		return imageService;
	}

	public AnnouncementService getAnnouncementService() {
		return announcementService;
	}
}
