package org.ayizan.android.fogguide.service;


public class ReferenceService {

	private static ReferenceService instance = new ReferenceService();
	
	public static ReferenceService instance() {
		return instance;
	}

	public ReferenceItem[] getReferenceItems() {
		return new ReferenceItem[] { 
				};
	}

	public ReferenceItem[] getSubMenu(String subMenuName) {
		ReferenceItem[] result = null;
		ReferenceItem[] items = getReferenceItems();
		for (ReferenceItem referenceItem : items) {
			if (referenceItem.getDescription().equals(subMenuName) && referenceItem.isSubMenu()) {
				result = referenceItem.getItems();
				break;
			}
		}
		return result;
	}
}
