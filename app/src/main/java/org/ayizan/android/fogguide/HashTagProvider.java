package org.ayizan.android.fogguide;

public interface HashTagProvider {
	public String getHashTag();
}
